<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes(); ?>>

<head profile="http://gmpg.org/xfn/11">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />

<title><?php bloginfo('name'); ?> <?php if ( is_single() ) { ?> &raquo; Blog Archive <?php } ?> <?php wp_title(); ?></title>
<link rel="stylesheet" type="text/css" href="<?php bloginfo( 'template_url' ); ?>/slide.css" />
<link rel="stylesheet" href="<?php bloginfo( 'template_url' ); ?>/mobile.css" media="only screen and (max-device width:480px)"/>
<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="screen" />
<link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?> RSS Feed" href="<?php bloginfo('rss2_url'); ?>" />
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />

<link href="http://fonts.googleapis.com/css?family=Roboto" rel="stylesheet" type="text/css">
<link href="http://fonts.googleapis.com/css?family=Oswald" rel="stylesheet" type="text/css">

<style type="text/css" media="screen">

<?php
// Checks to see whether it needs a sidebar or not
if ( !empty($withcomments) && !is_single() ) {
?>
	#page { background: url("<?php bloginfo('stylesheet_directory'); ?>/images/kubrickbg-<?php bloginfo('text_direction'); ?>.jpg") repeat-y top; border: none; }
<?php } else { // No sidebar ?>
	#page { background: url("<?php bloginfo('stylesheet_directory'); ?>/images/kubrickbgwide.jpg") repeat-y top; border: none; }
<?php } ?>

</style>

<?php wp_head(); ?>

<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-18288601-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>



<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js" ></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.5.3/jquery-ui.min.js" ></script>
<script type="text/javascript" src="http://www.geic.com.ar/2010/wp-content/plugins/wp-featured-content-slider/scripts/jquery.cycle.all.2.72.js" ></script>

<script type="text/javascript">
	$(document).ready(function(){
		$("#featured-slide > ul").tabs({fx:{opacity: "toggle"}}).tabs("rotate", 5000, true);
	});
</script>



</head>
<body>
	<div id="contenedor-sitio">
		<div id="menu-principal">
        	<div style="width: 70px; height: 24px; float: left;padding: 10px 15px;">
				<a href="<?php echo get_option('home'); ?>"><img src="<?php bloginfo( 'template_url' ); ?>/imagenes/geic-small.png" border="0"></a>
            </div>
            <div style="width: 880px; height: 45px; float: left;margin-left: 10px;">
                <ul>
                    <?php wp_list_categories('title_li=&include=3,4,5,6,7,8,9,11,13,14,15');?>
                </ul>
           	</div>
		</div>
		<div id="top">	
			<div id="menu-top">
				<ul>
					<li class="nada"><a href="<?php echo get_option('home'); ?>/about/">Quienes Somos</a></li>
                    <li><a href="<?php echo get_option('home'); ?>/recursos/">Memoria</a></li>
                    <li><a href="<?php echo get_option('home'); ?>/fundacion-ceic/">Agenda</a></li>
                    <li><a href="<?php echo get_option('home'); ?>/contacto/">Contacto</a></li>

						<!--<?php wp_list_pages('title_li='); ?>-->
				</ul>
			</div>
			<div style="float:right; width:555px; margin:0; padding:0;">
				<div id="buscador-top">
             		<?php get_search_form(); ?>
				</div>
        <div id="footer3Icon">
            <div id="icon-social">
                    <div style="width:26px; height:20px; float:left;">
                        <a class="socialMedia" href="<?php echo get_option('home'); ?>/contacto/"><img src="<?php bloginfo( 'template_url' ); ?>/imagenes/mail.jpg" alt="Contacto" border="0"></a>
                    </div>
                    <div style="width:26px; height:20px; float:left;">
                        <a class="socialMedia" href="http://www.facebook.com/pages/Grupo-de-Estudios-Internacionales-Contempor%C3%A1neos-GEIC/188026151274947" target="_blank"><img src="<?php bloginfo( 'template_url' ); ?>/imagenes/facebook.jpg" alt="GEIC en Facebook" border="0"></a>
                    </div>
                    <div style="width:26px; height:20px; float:left;">
                        <a class="socialMedia" href="http://twitter.com/#!/GEIC_" target="_blank"><img src="<?php bloginfo( 'template_url' ); ?>/imagenes/twitter.jpg" alt="GEIC en Twitter" border="0"></a>
                    </div>
                    <div style="width:26px; height:20px; float:left;">
                        <a class="socialMedia" href="http://feeds.feedburner.com/Geic-GrupoDeEstudiosInternacionalesContemporneos"><img src="<?php bloginfo( 'template_url' ); ?>/imagenes/rss.jpg" alt="GEIC RSS" border="0"></a>
                    </div>
             </div>
        </div>     
             
			</td>
			</div>
           </div>


          <!-- <div id="animacion">

            <object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,28,0" width="900" height="110">
              <param name="movie" value="<?php bloginfo( 'template_url' ); ?>/animacion/animacion.swf">
              <param name="quality" value="high">
              <embed src="<?php bloginfo( 'template_url' ); ?>/animacion/animacion.swf" quality="high" pluginspage="http://www.adobe.com/shockwave/download/download.cgi?P1_Prod_Version=ShockwaveFlash" type="application/x-shockwave-flash" width="900" height="110"></embed>
            </object>

           </div>-->
           
           
			


