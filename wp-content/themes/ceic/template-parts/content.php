<?php
/**
 * The template part for displaying content
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
?>
<?php
$classes = "col-xs-12";
?>
<article id="post-<?php the_ID(); ?>" <?php post_class($classes); ?>>
	<div class="row">
		<div class="col-md-3 col-md-offset-2 col-xs-12">
		<?php the_post_thumbnail('archive-size', array( 'class' => 'img-responsive' )); ?>
		</div>
		<div class="col-md-5 archive-post-content col-xs-12">
			<div class="row">
				<header class="entry-header col-xs-12">
					<?php the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>
				</header><!-- .entry-header -->
			</div>
			<div class="row">
				<div class="entry-content col-xs-12">
					<?php the_excerpt(); ?>
					<p class="author">-<br><?php $key="autor"; echo get_post_meta($post->ID, $key, true); ?></p>
				</div>
			</div>
		</div>
	</div>
</article><!-- #post-## -->
