<?php get_header(); ?>
  <div id="cuerpo-contenido">
  <div id="cuerpo">
	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
    
		<div id="salida-nota">
        	<?php mi_imagen($tamano = 'grande') ; ?>
        	<h4><?php the_time(__('j/m/y')); ?> - <?php $key="subtitulo"; echo get_post_meta($post->ID, $key, true); ?></h4>
			<h5><?php the_title(); ?></h5>
            

			
			<?php $key="autor"; echo get_post_meta($post->ID, $key, true); ?>
			
			<p><?php the_content('<p class="serif">Read the rest of this entry &raquo;</p>'); ?> </p>
        </div>
        
		<?php endwhile; else: ?>
			<p>Sorry, no posts matched your criteria.</p>
		<?php endif; ?>
          
		<?php comments_template( '', true ); ?>



	</div>


<?php get_footer(); ?>
