<div id="sidebar">
	<?php get_sidebar(); ?>
<br clear="all"/>
</div>
<br clear="all"/>
</div>

<div id="footer">
     <div id="footer2">
     <div class="col1">
     	<h2>Navegación</h2>
     	<ul>
        	<li><a href="<?php echo get_option('home'); ?>">Inicio</a></li>
            <li><a href="<?php echo get_option('home'); ?>/about/">Quienes Somos</a></li>
            <li><a href="<?php echo get_option('home'); ?>/objetivos/">Objetivos</a></li>
            <li><a href="<?php echo get_option('home'); ?>/recursos/">Memoria</a></li>
            <li><a href="<?php echo get_option('home'); ?>/agenda/">Agenda</a></li>
            <li><a href="<?php echo get_option('home'); ?>/publicar-en-geic/">Publicar</a></li>
            <li><a href="<?php echo get_option('home'); ?>/contacto/">Contacto</a></li>
        </ul>
     </div>
     <div class="col2">
     	<h2>Categorias</h2>
   	   	<ul>
        	<li><a href="<?php echo get_option('home'); ?>/category/economia-internacional/">Economía Internacional</a></li>
            <li><a href="<?php echo get_option('home'); ?>/category/energia-y-ambiente/">Energía y Ambiente</a></li>
         	<li><a href="<?php echo get_option('home'); ?>/category/estado-y-sociedad/">Estado y Sociedad</a></li>
            <li><a href="<?php echo get_option('home'); ?>/category/politica-internacional/">Política Internacional</a></li>
            <li><a href="<?php echo get_option('home'); ?>/category/seguridad-internacional/">Seguridad Internacional</a></li>
        </ul>
     </div>
     <div class="col3">
     	<h2>CEIC Social</h2>
   	   	<ul>
        	<li><a href="http://www.facebook.com/pages/Grupo-de-Estudios-Internacionales-Contempor%C3%A1neos-GEIC/188026151274947" target="_blank"><img src="<?php bloginfo( 'template_url' ); ?>/imagenes/facebook.gif" width="14" height="14" /> Facebook</a></li>
            <li><a href="https://twitter.com/CEIC_" target="_blank"><img src="<?php bloginfo( 'template_url' ); ?>/imagenes/twitter.gif" width="14" height="15" /> Twiiter</a></li>
            <li><a href="http://feeds.feedburner.com/Geic-GrupoDeEstudiosInternacionalesContemporneos" target="_blank"><img src="<?php bloginfo( 'template_url' ); ?>/imagenes/rss.gif" width="14" height="14" /> RSS</a></li>
            <li><a href="http://fundaceic.org/contacto/" target="_blank"><img src="<?php bloginfo( 'template_url' ); ?>/imagenes/email.gif" width="14" height="13" /> Contacto</a></li>
        </ul>
     </div>
     <div class="col4">
     	<p><img src="<?php bloginfo( 'template_url' ); ?>/imagenes/loguito.png" />
     	<br/> <br/>Montevideo 951 - C&oacute;rdoba - Argentina - 5000 - 0351 - 157010224<br/>e-mail: info@fundaceic.org<br/>ISSN 1853-1873</p>
        <img style="float:right; margin:0;" src="<?php bloginfo( 'template_url' ); ?>/imagenes/fundacion.png"/>
     </div>
     <br clear="all" />
</div>



</div>
<div id="footer3">
    <div style=" width:980px; margin:0 auto;">
        <div id="footer3Txt">
            <p>(c) 2011 - Todos los derechos reservados - Dise&ntilde;ado y desarrollado por <a href="http://www.waimaka.com" title="Waimaka" target="_blank">Waimaka.com</strong></a><br /></p>
        </div>
        
    </div>
</div>


<?php wp_footer(); ?>



</body>
</html>
