<?php
/* 
 * Plugin Name:   Max Banner Ads PRO
 * Version:       1.3.4
 * Plugin URI:    http://www.maxblogpress.com/plugins/mba/
 * Description:   Easily rotate banners and ads in almost anywhere in your wordpress blog without editing the theme. Adjust your settings <a href="edit.php?page=max-banner-ads-pro/max-banner-ads.php">here</a>.
 * Author:        MaxBlogPress
 * Author URI:    http://www.maxblogpress.com
 *
 * License: Copyright (c) 2008 Pawan Agrawal. All rights reserved.
 * 
 * This plugin uses a commercial script library.
 * 
 * Please refer to "license.txt" file located at "max-banner-ads-lib/"
 * for copyright notice and end user license agreement.
 * 
 */
 
$mban_path     = preg_replace('/^.*wp-content[\\\\\/]plugins[\\\\\/]/', '', __FILE__);
$mban_path     = str_replace('\\','/',$mban_path);
$mban_dir      = substr($mban_path,0,strrpos($mban_path,'/'));
$mban_siteurl  = get_bloginfo('wpurl');
$mban_siteurl  = (strpos($mban_siteurl,'http://') === false) ? get_bloginfo('siteurl') : $mban_siteurl;
$mban_fullpath = $mban_siteurl.'/wp-content/plugins/'.$mban_dir.'/';
$mban_relpath  = str_replace('\\','/',dirname(__FILE__));
$mban_libpath  = $mban_fullpath.'max-banner-ads-lib/';
$mban_abspath  = str_replace("\\","/",ABSPATH); 
define('MBAN_ABSPATH', $mban_abspath);
define('MBAN_PATH', $mban_path);
define('MBAN_FULLPATH', $mban_fullpath);
define('MBAN_LIBPATH', $mban_libpath);
define('MBAN_SITEURL', $mban_siteurl);
define('MBAN_NAME', 'Max Banner Ads PRO');
define('MBAN_VERSION', '1.3.4');
require_once($mban_relpath.'/max-banner-ads-lib/include/max-banner-ads.cls.php');

/**
 * MBPBannerAdsPlugin - Max Banner Ads PRO
 * Holds all the necessary functions and variables
 */
class MBPBannerAdsPlugin extends MBPBannerAds
{
	/**
	 * Constructor. Adds the plugins plugin actions/filters and gets the user defined options.
	 */
	function MBPBannerAdsPlugin() {
		global $table_prefix;
		$this->mban_zone_table    = $table_prefix.$this->mban_zone_table;
		$this->mban_banner_table  = $table_prefix.$this->mban_banner_table;
		$this->mban_options_table = $table_prefix.$this->mban_options_table;

		$this->usedbanners  = array();
		$this->img_how      = '<img src="'.MBAN_LIBPATH.'images/how.gif" border="0" align="absmiddle">';
		$this->img_comment  = '<img src="'.MBAN_LIBPATH.'images/comment.gif" border="0" align="absmiddle">';
		$this->img_remove   = '<img src="'.MBAN_LIBPATH.'images/remove.gif" border="0" align="absmiddle">';
		$this->img_home     = '<img src="'.MBAN_LIBPATH.'images/home.gif" border="0" align="absmiddle">';
		$this->img_add      = '<img src="'.MBAN_LIBPATH.'images/add.gif" border="0" align="absmiddle">';
		$this->img_add2     = '<img src="'.MBAN_LIBPATH.'images/add2.gif" border="0" align="absmiddle">';
		$this->img_edit     = '<img src="'.MBAN_LIBPATH.'images/edit.gif" border="0" align="absmiddle">';
		$this->img_delete   = '<img src="'.MBAN_LIBPATH.'images/delete.gif" border="0" align="absmiddle">';
		$this->img_reset    = '<img src="'.MBAN_LIBPATH.'images/reset.gif" border="0" align="absmiddle">';
		$this->img_child    = '<img src="'.MBAN_LIBPATH.'images/sub.gif" border="0" align="absmiddle">';
		$this->img_tag      = '<img src="'.MBAN_LIBPATH.'images/tag.gif" border="0" align="absmiddle" title="Template/Post Tags">';
		$this->img_warning  = '<img src="'.MBAN_LIBPATH.'images/warning.gif" border="0" align="absmiddle">';
		$this->img_active   = '<img src="'.MBAN_LIBPATH.'images/active.gif" border="0" align="absmiddle" title="Active (Click to make Inactive)">';
		$this->img_inactive = '<img src="'.MBAN_LIBPATH.'images/inactive.gif" border="0" align="absmiddle" title="Inactive (Click to make Active)">';

		add_action('activate_'.MBAN_PATH, array(&$this, 'mbanActivate'));
		add_action('init', array(&$this, 'mbanInit'));
		add_action('admin_head', array(&$this, 'mbanStyleAndJS'));
		add_action('admin_menu', array(&$this, 'mbanAddMenu'));
		add_action('after_plugin_row', array(&$this, 'mbanCheckPluginVersion'));
		add_action('loop_start', array(&$this, 'mbanInsertBannerInTFP'));
		add_action('loop_end', array(&$this, 'mbanInsertBannerInBLP'));
		add_filter('the_content', array(&$this, 'mbanInsertBannerWithinPost'));
		add_filter('the_excerpt', array(&$this, 'mbanWithinPostExcerpt'));
	}
	
	/**
	 * Called when plugin is activated. Adds the plugins options to the options table.
	 */
	function mbanActivate() {
		$ret1 = $this->__mbanCreateZoneTable();
		$ret2 = $this->__naffCreateBannerTable();
		$ret3 = $this->__mbanCreateOptionsTable();
		if ( $ret1 == true && $ret3 == true ) $this->__mbanAddDefaultData();
		else $this->__mbanAddDefaultData('pro'); 
		if ( $ret1 == false || $ret2 == false ) $this->__mbanAlterTables(DB_NAME);
		update_option('mban_version', MBAN_VERSION);
		return true;
	}
	
	/**
	 * Plugin's stylesheet and javascripts
	 */
	function mbanStyleAndJS() {
		$this->__mbanStyleAndJS();
	}
	
	/**
	 * Plugin Initialization
	 */
	function mbanInit() {
		$this->mban_init = 1;
	}
	
	/**
	 * Adds the plugins link in admin's Manage menu
	 */
	function mbanAddMenu() {
		add_management_page(MBAN_NAME, 'Max Banner Ads PRO', 9, MBAN_PATH, array(&$this, 'mbanOptionsPg'));
	}
	
	/**
	 * Gets recheck data fro displaying auto upgrade information
	 */
	function mbanRecheckData($data='') {
		if ( $data != '' ) {
			update_option('mbanpro_version_check',$data);
		} else {
			$version_chk = get_option('mbanpro_version_check');
			return $version_chk;
		}
	}
	
	/**
	 * Extracts plugin update data
	 */
	function mbanExtractUpdateData() {
		$arr = array();
		$version_chk_file = "http://www.maxblogpress.com/plugin-updates/max-banner-ads-pro.php?v=".MBAN_VERSION;
		$content = wp_remote_fopen($version_chk_file);
		if ( $content ) {
			$content          = nl2br($content);
			$content_arr      = explode('<br />', $content);
			$latest_version   = trim(trim(strstr($content_arr[0],'~'),'~'));
			$recheck_interval = trim(trim(strstr($content_arr[1],'~'),'~'));
			$download_url     = trim(trim(strstr($content_arr[2],'~'),'~'));
			$msg_plugin_mgmt  = trim(trim(strstr($content_arr[3],'~'),'~'));
			$msg_in_plugin    = trim(trim(strstr($content_arr[4],'~'),'~'));
			$upgrade_url      = MBAN_SITEURL.'/wp-admin/edit.php?page='.MBAN_PATH.'&action=upgrade&dnl='.$download_url;
			$arr = array($latest_version, $recheck_interval, $download_url, $msg_plugin_mgmt, $msg_in_plugin, $upgrade_url);
		}
		return $arr;
	}
		
	/**
	 * Checks the plugin version and displays the message if new version is available
	 */
	function mbanCheckPluginVersion($plugin) {
		$update_arr = array();
		if ( strpos(MBAN_PATH,$plugin) !== false ) {	
			// Check for last checked date and access version info data only if recheck interval has reached
			$mban_version_chk = $this->mbanRecheckData();
			if ( ($mban_version_chk == '') || strtotime(date('Y-m-d H:i:s')) > (strtotime($mban_version_chk['last_checked_on']) + $mban_version_chk['recheck_interval']*60*60) ) {
				$update_arr = $this->mbanExtractUpdateData();
				if ( count($update_arr) > 0 ) {
					$latest_version   = $update_arr[0];
					$recheck_interval = $update_arr[1];
					$download_url     = $update_arr[2];
					$msg_plugin_mgmt  = $update_arr[3];
					$msg_in_plugin    = $update_arr[4];
					$upgrade_url      = $update_arr[5];
					if( MBAN_VERSION < $latest_version ) {
						$mban_version_check = array('recheck_interval' => $recheck_interval, 'last_checked_on' => date('Y-m-d H:i:s'));
						$this->mbanRecheckData($mban_version_check);
						$msg_plugin_mgmt = str_replace("%latest-version%", $latest_version, $msg_plugin_mgmt);
						$msg_plugin_mgmt = str_replace("%plugin-name%", MBAN_NAME, $msg_plugin_mgmt);
						$msg_plugin_mgmt = str_replace("%upgrade-url%", $upgrade_url, $msg_plugin_mgmt);
						echo '<td colspan="5" class="plugin-update" style="line-height:2.5em;">'.$msg_plugin_mgmt.'</td>';
					} else {
						$msg_plugin_mgmt = '';
					}
				}
			}
		}
	}
	
	/**

	 * Checks the page type
	 */
	function mbanCheckPageType() {
		$show_in = '';
		if ( is_home() ) $show_in = 'hom';
		if ( is_single() || is_page() ) $show_in = 'sin';
		if ( is_search() ) $show_in = 'sea';
		if ( is_archive() ) {
			if ( is_category() )  $show_in = 'cat';
			else if ( !is_tag() ) $show_in = 'arc';
		}
		return $show_in;
	}
	
	/**
	 * Displays banners in top of the first post
	 */
	function mbanInsertBannerInTFP() {
		if ( !is_admin() && !is_feed() && $this->mban_init ) {
			$banner_output = '';
			$banner_output = $this->__mbanInsertBanner('tfp');
			if ( strlen($banner_output) > 5 ) {
				echo $banner_output;
			}
		}
	}
	
	/**
	 * Displays banners in bottom of the last post
	 */
	function mbanInsertBannerInBLP() {
		if ( !is_admin() && !is_feed() && $this->mban_init ) {
			$banner_output = '';
			$banner_output = $this->__mbanInsertBanner('blp');
			if ( strlen($banner_output) > 5 ) {
				echo $banner_output;
			}
		}
	}
	
	/**
	 * Remove 'Powered by' text for wordpress post excerpt
	 */
	function mbanWithinPostExcerpt($post_excerpt) {
		if ( strpos($post_excerpt,'Powered by Max Banner Ads') !== false ) {
			$post_excerpt = str_replace('&nbsp;Powered by Max Banner Ads&nbsp;','',$post_excerpt);
			$post_excerpt = str_replace('Powered by Max Banner Ads','',$post_excerpt);
		}
		return $post_excerpt;
	}
	
	/**
	 * Displays banners within the post
	 */
	function mbanInsertBannerWithinPost($post_content) { 
		global $post, $paged, $posts_per_page;
		$this->post_number++;
		$post_content_orig = $post_content;
		$curr_pg = intval($paged) > 0 ? ($paged-1) : 0;
		$page_start_no = $curr_pg * $posts_per_page + 1;
		$post_number   = $page_start_no + ($this->post_number - 1);
		// One time fetch
		if ( trim($this->within_posts) == '' ) {
			$sql = "SELECT wp_show_in FROM $this->mban_zone_table WHERE within_post='1'";
			$rs = mysql_query($sql);
			while ( $row = mysql_fetch_assoc($rs) ) {
				$this->within_posts .= ','.$row['wp_show_in'].',';
			}
		}
		// Execute banner query only for required posts
		if ( strpos($this->within_posts,','.$post_number.',') !== false || strpos($this->within_posts,'all') !== false ) {
			$post_content = $this->__mbanInsertBanner('wp','',$post_content,$post_number);
		} 
		if ( $this->options_global['disable_all_banners'] != 1 ) {
			$post_content = $this->__mbanSearchPostTag($post_content,'zone'); // Check for zone tag
			$post_content = $this->__mbanSearchPostTag($post_content,'banner'); // Check for banner tag
			return $post_content;
		} else {
			return $post_content_orig;
		}
	}
	
	/**
	 * Counts Impression for each banner appearing on a page
	 */
	function mbanCountImpression() {
		if ( !is_admin() && !is_feed() && !is_user_logged_in() ) {
			$this->__mbanCountImpression();
		}
	}
	
	/**
	 * Displays the plugins options
	 */
	function mbanOptionsPg() {
		$mban_msg = '';
		$mban_msg = $this->__mbanOptionsPg();
		if ( trim($mban_msg) != '' ) {
			echo '<div id="message" class="updated fade"><p><strong>'.$mban_msg.'</strong></p></div>';
		}
		echo '<div class="wrap">';
		$this->__mbanShowOptionsPg();
		echo '</div>';
	}
	
	/**
	 * Interface for Adding/Editing Zone
	 */
	function mbanAddEditZone($msg='') {
		if ( trim($msg) != '' ) {
			echo '<div id="message" class="updated fade"><p><strong>'.$msg.'</strong></p></div>';
		}
		echo '<div class="wrap">';
		$this->__mbanAddEditZone();
		echo '</div>';
	}
	
	/**
	 * Interface for Adding/Editing Banner
	 */
	function mbanAddEditBanner($msg='') {
		if ( trim($msg) != '' ) {
			echo '<div id="message" class="updated fade"><p><strong>'.$msg.'</strong></p></div>';
		}
		echo '<div class="wrap">';
		$this->__mbanAddEditBanner();
		echo '</div>';
	}

} // Eof Class

$MBPBannerAdsPlugin = new MBPBannerAdsPlugin();
add_action('plugins_loaded', 'mbanWidgetInit');

/**
 * Template Tag. Displays banners in a zone
 */
function mba_display_zone($zone_id) {
	global $MBPBannerAdsPlugin;
	if ( !is_admin() ) {
		if ( intval($zone_id) > 0 ) {
			$zone_filter = "t1.id='$zone_id'";
		} else {
			$zone_filter = "t1.name='$zone_id'";
		}
		$sql = "SELECT t2.id, t2.url, t2.link, t2.in_new_win, t2.ad_type, t2.text_ad_code, t1.id as zoneID, t1.noof_banners, 
				t1.banner_prefix_suffix, t1.banner_prefix, t1.banner_suffix, t1.banner_style_class 
				FROM $MBPBannerAdsPlugin->mban_zone_table t1 INNER JOIN $MBPBannerAdsPlugin->mban_banner_table t2 ON t1.id=t2.zoneid 
				WHERE {$zone_filter} AND t1.zone_disable<>'1' AND t2.status='1' ORDER BY RAND()";
		$rs   = mysql_query($sql);
		$cnt  = 0;
		$id   = array();
		$url  = array();
		$link = array();
		$in_new_win = array();
		$text_ad_code = array();
		$zone_properties = array();
		while ( $data = mysql_fetch_assoc($rs) ) {
			$cnt++;
			if ( $data['ad_type'] == 1 ) {
				$id[] = $data['id'];
				$text_ad_code[] = $data['text_ad_code'];
			} else {
				$id[]   = $data['id'];
				$url[]  = $data['url'];
				$link[] = $data['link'];
				$in_new_win[] = $data['in_new_win'];
			}
			if ( count($zone_properties) <= 0 ) {
				$zone_properties = array($data['banner_prefix_suffix'],$data['banner_prefix'],$data['banner_suffix'],$data['banner_style_class']);
			}
			if ( $cnt >= $data['noof_banners'] ) break;
		}
		if ( count($id) > 0 ) {
			$banner_output = $MBPBannerAdsPlugin->__mbanGetBanner($id,$url,$link,$in_new_win,$zone_properties,$text_ad_code);
			echo $banner_output;
		}
	}
}

/**
 * Template Tag. Displays banner
 */
function mba_display_banner($banner_id) {
	global $MBPBannerAdsPlugin;
	if ( !is_admin() ) {
		if ( intval($banner_id) > 0 ) {
			$banner_filter = "id='$banner_id'";
		} else {
			$banner_filter = "name='$banner_id'";
		}
		$sql = "SELECT id,url,link,in_new_win,ad_type,text_ad_code FROM $MBPBannerAdsPlugin->mban_banner_table WHERE status='1' AND {$banner_filter}";
		$rs  = mysql_query($sql);
		$id   = array();
		$url  = array();
		$link = array();
		$in_new_win = array();
		$text_ad_code = array();
		$zone_properties = array();
		while ( $data = mysql_fetch_assoc($rs) ) {
			$cnt++;
			if ( $data['ad_type'] == 1 ) {
				$id[] = $data['id'];
				$text_ad_code[] = $data['text_ad_code'];
			} else {
				$id[]   = $data['id'];
				$url[]  = $data['url'];
				$link[] = $data['link'];
				$in_new_win[] = $data['in_new_win'];
			}
		}
		if ( count($id) > 0 ) {
			$banner_output = $MBPBannerAdsPlugin->__mbanGetBanner($id,$url,$link,$in_new_win,$zone_properties,$text_ad_code);
			echo $banner_output;
		}
	}
}

/**
 * Template Tag. Displays banners in sidebar
 */
function mban_sidebar_banners($zone_id='') {
	global $MBPBannerAdsPlugin;
	if ( !is_admin() ) {
		$banner_output = '';
		$banner_output = $MBPBannerAdsPlugin->__mbanInsertBanner('aw', $zone_id);
		if ( strlen($banner_output) > 5 ) {
			echo $banner_output;
		}
	}
}

/**
 * Max Banner Ads Widget
 */
function mbanWidgetInit() {
	global $MBPBannerAdsPlugin;
	// Check if required Widget API functions are defined
	if ( !function_exists('register_sidebar_widget') || !function_exists('register_widget_control') ) {
		return; 
	}

	function mbanWidgetSidebar($args,$zone_id) {
		mban_sidebar_banners($zone_id);
	}
	
	function mbanWidgetController($zone_id) {
		global $MBPBannerAdsPlugin;
		if ( isset($_POST["mban_widget_submit"]) ) {
			foreach ( $_POST['mban'] as $zoneID => $zone_data ) {
				$aw_title                  = $zone_data['aw_title'];
				$aw_position               = $zone_data['aw_position'];
				$aw_position_fixed         = $zone_data['aw_position_fixed'];
				$aw_position_random_left   = $zone_data['aw_position_random_left'];
				$aw_position_random_center = $zone_data['aw_position_random_center'];
				$aw_position_random_right  = $zone_data['aw_position_random_right'];
				$aw_position_custom_prefix = $zone_data['aw_position_custom_prefix'];
				$aw_position_custom_suffix = $zone_data['aw_position_custom_suffix'];
				$sql = "UPDATE $MBPBannerAdsPlugin->mban_zone_table SET aw_position='$aw_position', aw_position_fixed='$aw_position_fixed', aw_position_random_left='$aw_position_random_left', 
						aw_position_random_center='$aw_position_random_center', aw_position_random_right='$aw_position_random_right', aw_position_custom_prefix='$aw_position_custom_prefix', 
						aw_position_custom_suffix='$aw_position_custom_suffix', aw_title='$aw_title'  
						WHERE id='$zoneID'";
				mysql_query($sql);
			}
		}
		$sql = "SELECT * FROM $MBPBannerAdsPlugin->mban_zone_table WHERE id='$zone_id'";
		$rs = mysql_query($sql);
		if ( $rs > 0 ) {
			$row = mysql_fetch_assoc($rs);
			$aw_title                  = $row['aw_title'];
			$aw_position               = $row['aw_position'];
			$aw_position_fixed         = $row['aw_position_fixed'];
			$aw_position_random_left   = $row['aw_position_random_left'];
			$aw_position_random_center = $row['aw_position_random_center'];
			$aw_position_random_right  = $row['aw_position_random_right'];
			$aw_position_custom_prefix = $row['aw_position_custom_prefix'];
			$aw_position_custom_suffix = $row['aw_position_custom_suffix'];
		}
		$row_display_style = strpos($_SERVER['HTTP_USER_AGENT'],'MSIE') ? 'block' : 'table-row';
		$aw_position_custom_display = 'none';
		$aw_position_random_display = 'none';
		$aw_position_fixed_display  = 'none';
		if ( $aw_position == 'custom' ) {
			$aw_position_custom_display = $row_display_style;
			$aw_position_custom_chk = 'checked';
		} else if ( $aw_position == 'random' ) {
			$aw_position_random_display = $row_display_style;
			$aw_position_random_chk = 'checked';
		} else {
			$aw_position_fixed_display  = $row_display_style;
			$aw_position_fixed_chk = 'checked';
		}
		if ( $aw_position_fixed == 'left' ) $aw_position_fixed_left = 'selected';
		else if ( $aw_position_fixed == 'right' ) $aw_position_fixed_right = 'selected';
		else $aw_position_fixed_center = 'selected';
		if ( $aw_position_random_left == 1 )   $aw_position_random_left_chk   = 'checked';
		if ( $aw_position_random_center == 1 ) $aw_position_random_center_chk = 'checked';
		if ( $aw_position_random_right == 1 )  $aw_position_random_right_chk  = 'checked';
		?>
		<div>
		  <table cellpadding="3" cellspacing="0" align="right" border="0" width="100%" style="border:1px solid #f1f1f1">
		   <tr bgcolor="#f3f3f3">
		    <td>Title: <input type="text" name="mban[<?php echo $zone_id;?>][aw_title]" id="aw_title" value="<?php echo htmlspecialchars($aw_title);?>" size="30" /></td>
		   </tr>
		   <tr bgcolor="#ffffff">
			<td><input type="radio" name="mban[<?php echo $zone_id;?>][aw_position]" id="aw_position" value="fixed" <?php echo $aw_position_fixed_chk;?> onclick="__mbanShowHideAW('aw_position_fixed_div','<?php echo $zone_id;?>')" /> Fixed Alignment</td>
		   </tr>
		   <tr bgcolor="#ffffff" id="aw_position_fixed_div_<?php echo $zone_id;?>" style="display:<?php echo $aw_position_fixed_display;?>">
			<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
			<select name="mban[<?php echo $zone_id;?>][aw_position_fixed]" id="aw_position_fixed">
			 <option value="left" <?php echo $aw_position_fixed_left;?>>Left</option>
			 <option value="center" <?php echo $aw_position_fixed_center;?>>Center</option>
			 <option value="right" <?php echo $aw_position_fixed_right;?>>Right</option>
			</select></td>
		   </tr>
		   <tr bgcolor="#f3f3f3">
			<td><input type="radio" name="mban[<?php echo $zone_id;?>][aw_position]" id="aw_position_random" value="random" <?php echo $aw_position_random_chk;?> onclick="__mbanShowHideAW('aw_position_random_div','<?php echo $zone_id;?>')" /> Random</td>
		   </tr>
		   <tr bgcolor="#f3f3f3" id="aw_position_random_div_<?php echo $zone_id;?>" style="display:<?php echo $aw_position_random_display;?>">
			<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
			<input type="checkbox" name="mban[<?php echo $zone_id;?>][aw_position_random_left]" value="1" id="aw_position_random" <?php echo $aw_position_random_left_chk;?>  /> Left &nbsp;&nbsp;&nbsp; 
			<input type="checkbox" name="mban[<?php echo $zone_id;?>][aw_position_random_center]" value="1" id="aw_position_random" <?php echo $aw_position_random_center_chk;?>  /> Center &nbsp;&nbsp;&nbsp; 
			<input type="checkbox" name="mban[<?php echo $zone_id;?>][aw_position_random_right]" value="1" id="aw_position_random" <?php echo $aw_position_random_right_chk;?>  /> Right	    </td>
		   </tr>
		   <tr bgcolor="#ffffff">
			<td><input type="radio" name="mban[<?php echo $zone_id;?>][aw_position]" id="aw_position_custom" value="custom" <?php echo $aw_position_custom_chk;?> onclick="__mbanShowHideAW('aw_position_custom_div','<?php echo $zone_id;?>')" /> Custom </td>
		   </tr>
		   <tr bgcolor="#ffffff" id="aw_position_custom_div_<?php echo $zone_id;?>" style="display:<?php echo $aw_position_custom_display;?>">
			<td> 
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Before Code: <input type="text" name="mban[<?php echo $zone_id;?>][aw_position_custom_prefix]" id="aw_position_custom_prefix" value="<?php echo htmlspecialchars($aw_position_custom_prefix);?>" size="15" /><br /> 
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;After Code:&nbsp;&nbsp; <input type="text" name="mban[<?php echo $zone_id;?>][aw_position_custom_suffix]" id="aw_position_custom_suffix" value="<?php echo htmlspecialchars($aw_position_custom_suffix);?>" size="15" /></td>
		   </tr>
		   <tr bgcolor="#f3f3f3" height="12"><td></td></tr>
		   <tr bgcolor="#f3f3f3">
		    <td align="center">Powered by <a href="http://www.maxblogpress.com/" target="_blank"><?php echo MBAN_NAME;?> Plugin</a>
			<input type="hidden" name="mban[<?php echo $zone_id;?>][widget_options_exist]" id="mban[<?php echo $zone_id;?>][widget_options_exist]" value="<?php echo $num_rows;?>" />
			<input type="hidden" name="mban_widget_submit" id="mban_widget_submit" value="1" /></td>
		   </tr>
		  </table>
		</div>
	<?php
	}
	
	$sql = "SELECT t1.id, t1.name FROM $MBPBannerAdsPlugin->mban_zone_table t1 INNER JOIN $MBPBannerAdsPlugin->mban_banner_table t2 
			ON t1.id=t2.zoneid WHERE t1.zone_disable<>'1' AND t1.as_widget='1' AND t2.status='1' ORDER BY t1.id DESC";
	$rs  = mysql_query($sql);
	if ( $rs ) {
		while ( $row = mysql_fetch_assoc($rs) ) {
			$zone_id   = $row['id'];
			$zone_name = 'MBAN-'.$row['name'];
			if ( function_exists('wp_register_sidebar_widget') ) { // fix for wordpress 2.2
				wp_register_sidebar_widget(sanitize_title($zone_name), $zone_name, 'mbanWidgetSidebar', array(), $zone_id);
			} else {
				register_sidebar_widget($zone_name, 'mbanWidgetSidebar', $zone_id);
			}
			register_widget_control($zone_name, 'mbanWidgetController', '', '210px', $zone_id);
		}
	}
}
?>