	<footer>
		<div class="container">
			<div class="row footer-widget">
				<div class="col-md-2 hidden-xs">
				<?php if ( is_active_sidebar( 'footer-1' ) ) : ?>
						<?php dynamic_sidebar( 'footer-1' ); ?>
				<?php endif; ?>
				</div>
				<div class="col-md-8 text-center">
					<?php if ( has_nav_menu( 'footer' ) ) : ?>
							<nav id="footer-navigation" class="footer-navigation" role="navigation" aria-label="Footer Links Menu">
								<?php wp_nav_menu( array( 
									  	'theme_location' 	=> 'footer',
									  	'container' 		=> '',
									  	'menu_class'      	=> 'nav navbar-nav',
									  	'menu_id'         	=> '',
									  	'echo'            	=> true,
									  	'fallback_cb'     	=> 'wp_page_menu',
									  	'before'          	=> '',
									  	'after'           	=> '',
									  	'link_before'     	=> '',
									  	'link_after'      	=> '',
									  	'items_wrap'      	=> '<ul id="%1$s" class="%2$s">%3$s</ul>',
									  	'depth'           	=> 0,
									  	'walker'          	=> ''
								)); ?>
							</nav><!-- .social-navigation -->
						<?php endif; ?>
				</div>
				<div class="col-md-2">
				<?php if ( is_active_sidebar( 'footer-2' ) ) : ?>
						<?php dynamic_sidebar( 'footer-2' ); ?>
				<?php endif; ?>
				</div>
				<div class="col-md-2 visible-xs-block text-center">
				<?php if ( is_active_sidebar( 'footer-1' ) ) : ?>
						<?php dynamic_sidebar( 'footer-1' ); ?>
				<?php endif; ?>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12 text-center copy">
					(c) 2016 - Todos los derechos reservados
				</div>
			</div>
		</div>
	</footer>
	<!-- Latest compiled and minified JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
	<?php wp_footer(); ?>
	<script type="text/javascript">
		jQuery(function(){
			jQuery(".navbar-toggle").on("click", function () {
			    jQuery("#hola").toggleClass("active");
			});

			jQuery(document).on('click', '.expand-contract-click', function(){

				jQuery('.expand-contract').toggle();
				jQuery('#expand').toggle();
				jQuery('#contract').toggle();

			});

			jQuery('.home-box').each(function(index){
				if(index > 1){
					jQuery(this).addClass('hidden-xs');
				}
			});
		});
	</script>

	<?php 
	if(is_front_page()){?>
	<script type="text/javascript">
		jQuery("#owl-institucional").owlCarousel({
	        items: 3,
	        autoPlay: true,
	        autoplayHoverPause : true,
	        nav : true,
	        dots: true,
	        navText: ["<",">"]
	      });
	</script>
	<?php } ?>
	<!-- Go to www.addthis.com/dashboard to customize your tools -->
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-57ebbb59020775b8"></script>
</body>
</html>