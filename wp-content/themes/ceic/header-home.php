<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php endif; ?>
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	<?php wp_head(); ?>
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script type="text/javascript">
    	jQuery(function(){
			
				jQuery('.content_left').css('left', jQuery('.navbar-logo').offset().left);
				var tamanio_slider = jQuery('.container-main article:nth-child(2)').width() + jQuery('.container-main article:nth-child(2)').offset().left + 30;
				jQuery('.home-slider').width(tamanio_slider);
				jQuery('.home-pager').width(jQuery(window).width()-tamanio_slider);
			jQuery(window).resize(function(){
				jQuery('.content_left').css('left', jQuery('.navbar-logo').offset().left);
				var tamanio_slider = jQuery('.container-main article:nth-child(2)').width() + jQuery('.container-main article:nth-child(2)').offset().left + 30;
				jQuery('.home-slider').width(tamanio_slider);
				jQuery('.home-pager').width(jQuery(window).width()-tamanio_slider);
			});
    		jQuery('#featured_slider ul').cycle({ 
				fx: '<?php $c_slider_effect = get_option('effect'); if(!empty($c_slider_effect)) {echo $c_slider_effect;} else {echo "scrollLeft";}?>',
				prev: '.feat_prev',
				next: '.feat_next',
				speed:  800, 
				timeout: <?php $c_slider_timeout = get_option('timeout'); if(!empty($c_slider_timeout)) {echo $c_slider_timeout;} else {echo 4000;}?>, 
				pager:  '.feat_pager'
			});
    	});
    </script>
</head>

<body <?php body_class(); ?>>
<?php get_header('side'); ?>
<div class="container-fluid">
	<div class="row">
		<div class="col-md-8 n-p home-slider">
			<?php

$c_slider_direct_path =  get_bloginfo('wpurl')."/wp-content/plugins/wp-featured-content-slider";

$c_slider_class = c_slider_get_dynamic_class();

?>

<div id="featured_slider">
	

	<ul id="slider">

		<?php
		
		$c_slider_sort = get_option('sort'); if(empty($c_slider_sort)){$c_slider_sort = "post_date";}
		$c_slider_order = get_option('order'); if(empty($c_slider_order)){$c_slider_order = "DESC";}
		$c_slider_limit = get_option('limit'); if(empty($c_slider_limit)){$c_slider_limit = 350;}
		$c_slider_points = get_option('points'); if(empty($c_slider_points)){$c_slider_points = "...";}
		$c_slider_post_limit = get_option('limit_posts'); if(empty($c_slider_limit_posts)){$c_slider_limit_posts = "-1";}
                
		global $wpdb;
	
		global $post;
		
		$args = array( 'meta_key' => 'feat_slider', 'meta_value'=> '1', 'suppress_filters' => 0, 'post_type' => array('post', 'page'), 'orderby' => $c_slider_sort, 'order' => $c_slider_order, 'numberposts'=> $c_slider_post_limit);
		
		$myposts = get_posts( $args );

		$categoriesMayor = array(15,6,9,11);

		foreach( $myposts as $post ) :	setup_postdata($post);
			
			$c_slider_custom = get_post_custom($post->ID);
			
			$c_slider_thumb = c_slider_get_thumb("feat_slider");

			$categories = get_the_category();


			$rl_category_color = '#4c4c4c';
			foreach($categories as $category){
	
				if(in_array($category->cat_ID, $categoriesMayor)){
					$rl_category_color = rl_color($category->cat_ID);
				}
			}
			
		?>
		
		<li>
			<?php $key="feat_slider_box"; $feat_slider_box = (get_post_meta($post->ID, $key, true) !== '0')?1:0; ?>
            <div class="content_left" style="background-color: 
				<?php echo $rl_category_color;?>;
				<?php echo ($feat_slider_box == 0)?'display:none':'';?>">
                <h1>
                	<a href="<?php the_permalink();?>"><?php the_title();?></a>
                </h1>
                <h6 class="author">-<br><?php $key="autor"; echo get_post_meta($post->ID, $key, true); ?></h6>
                <p>
                	<a href="<?php the_permalink();?>">Leer Más</a>
                </p>
            </div>
            <a href="<?php the_permalink();?>">
	            <div class="img_right">
	                <?php the_post_thumbnail('slider-size'); ?>
	            </div>
            </a>
        </li>
		
		<?php endforeach; ?>
	
	</ul>
	
</div>









		</div>
		<div class="col-md-4 n-p home-pager" style="height: 580px;background: url(<?php echo  content_url(); ?>/uploads/2016/09/Fondo-banderas-ceic-02.png);position: relative;z-index: 1000;">
			<div class="navbar-pager-box">
				<div class="feat_prev"><</div>
				<div class="feat_pager"></div>
				<div class="feat_next">></div>
			</div>
		</div>
	</div>
</div>
<div class="container-fluid navbar-superior navbar-home">
	<div class="row">
		<div class="container">
			<div class="row">
				<div class="col-md-2 col-xs-4 navbar-search visible-xs-block visible-sm-block"><?php get_search_form(true); ?></div>
				<div class="col-md-1 navbar-logo col-xs-4">
					<?php ceic_logo();?>
				</div>
				<div class="col-md-7 hidden-xs hidden-sm">
					<?php if ( has_nav_menu( 'superior' ) ) : ?>
					<?php wp_nav_menu( array( 
						  	'theme_location' 	=> 'superior',
						  	'container' 		=> '',
						  	'menu_class'      	=> 'nav navbar-nav col-md-12',
						  	'menu_id'         	=> '',
						  	'echo'            	=> true,
						  	'fallback_cb'     	=> 'wp_page_menu',
						  	'before'          	=> '',
						  	'after'           	=> '',
						  	'link_before'     	=> '',
						  	'link_after'      	=> '',
						  	'items_wrap'      	=> '<ul id="%1$s" class="%2$s">%3$s</ul>',
						  	'depth'           	=> 0,
						  	'walker'          	=> ''
					)); ?>
			<?php endif; ?>
				</div>
				<div class="col-md-2 navbar-search hidden-xs hidden-sm"><?php get_search_form(true); ?></div>
				<div class="col-md-1 col-xs-4 toggle-menu-button">
					<button type="button" class="navbar-toggle">
			          <span class="sr-only">Toggle navigation</span>
			          <span class="icon-bar"></span>
			          <span class="icon-bar"></span>
			          <span class="icon-bar"></span>
			        </button>
				</div>
				<div class="col-md-7 col-xs-12 visible-xs-block visible-sm-block">
					<?php if ( has_nav_menu( 'superior' ) ) : ?>
					<?php wp_nav_menu( array( 
						  	'theme_location' 	=> 'superior',
						  	'container' 		=> '',
						  	'menu_class'      	=> 'nav navbar-nav col-md-12',
						  	'menu_id'         	=> '',
						  	'echo'            	=> true,
						  	'fallback_cb'     	=> 'wp_page_menu',
						  	'before'          	=> '',
						  	'after'           	=> '',
						  	'link_before'     	=> '',
						  	'link_after'      	=> '',
						  	'items_wrap'      	=> '<ul id="%1$s" class="%2$s">%3$s</ul>',
						  	'depth'           	=> 0,
						  	'walker'          	=> ''
					)); ?>
			<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
</div>