<?php 

/*

Plugin Name: Facebook Share Button

Plugin URI: http://www.kumsaati.org/

Description: Adds a button which allows you to share post on Facebookshare. 

License: GNU General Public License 2.0 (GPL) http://www.gnu.org/licenses/gpl.html

Version: 1.0

Author: Kumsaati.org

Author URI: http://www.kumsaati.org

*/

if ( !defined('FACEBOOKSHARE_URL') ) {

	define('FACEBOOKSHARE_URL',get_option('siteurl').'/wp-content/plugins/'.plugin_basename(dirname(__FILE__)).'/');

} else {

	define('FACEBOOKSHARE_URL',WP_CONTENT_URL.'/plugins/'.plugin_basename(dirname(__FILE__)).'/');

}


function facebook_share_button($content) {
	global $post;
    $url = '';
    if (get_post_status($post->ID) == 'publish') {
    $url = get_permalink();
	$title = get_the_title($post->ID);
    }	

	if (get_option('facebook_share_where')=='manual' && get_option('facebook_share_style')!=''){
$button = '<div id="facebook_share_share_1" style="'.get_option('facebook_share_style').'">

<a name="fb_share" type="box_count" share_url="'.$url.'"></a>
<script src="http://static.ak.fbcdn.net/connect.php/js/FB.Share" type="text/javascript"></script>
 
</div>';
	} else {
	$button = '<div id="facebook_share_share_1" style="float: right; margin-right: 2px; margin-left: 2px">
<a name="fb_share" type="box_count" share_url="'.$url.'"></a>
<script src="http://static.ak.fbcdn.net/connect.php/js/FB.Share" type="text/javascript"></script>


</div>';
	}	

			if (get_option('facebook_share_where') == 'beforeandafter') {
				return $button . $content . $button;
			} else if (get_option('facebook_share_where') == 'before') {
				return $button . $content;
			} else {
				return $content . $button;
			}


}


add_filter('the_content', 'facebook_share_button');
add_filter('the_excerpt', 'facebook_share_button');

function facebookshare_options() {
	add_menu_page('Facebook Share', 'Facebook Share', 8, basename(__FILE__), 'facebook_share_options_page');
	add_submenu_page(basename(__FILE__), 'Settings', 'Settings', 8, basename(__FILE__), 'facebook_share_options_page');
}


if(is_admin()){
    add_action('admin_menu', 'facebookshare_options');
    add_action('admin_init', 'facebook_share_init');
}

function facebook_share_init(){
    if(function_exists('register_setting')){       
        register_setting('facebookshare-options', 'facebook_share_where');       
		register_setting('facebookshare-options', 'facebook_share_style');       
    }
}

function facebook_share_activate(){
    add_option('facebook_share_where', 'before');
    add_option('facebook_share_style', 'before');
	
}

function facebook_share_options_page() {
?>
<div style="padding:50px;">
<h2>Settings for Facebook Share button Integration in your blog</h2>
			<p>This plugin will install Facebook Share button in page and post. This plugin will provide you more updated features.  </p>
			<form method="post" action="options.php">
			<?php
				// New way of setting the fields, for WP 2.7 and newer
				if(function_exists('settings_fields')){
					settings_fields('facebookshare-options');
				} else {
					wp_nonce_field('update-options');?>

					<input type="hidden" name="action" value="update" />
            <input type="hidden" name="page_options" value="facebook_share_where" />
            <?php }?> Display Position<br>
                		<select name="facebook_share_where" onchange="if(this.value == 'manual'){getElementById('manualhelp').style.display = 'block';} else {getElementById('manualhelp').style.display = 'none';}">

                			<option <?php if (get_option('facebook_share_where') == 'before') echo 'selected="selected"'; ?> value="before">Before</option>

                			<option <?php if (get_option('facebook_share_where') == 'after') echo 'selected="selected"'; ?> value="after">After</option>

                			<option <?php if (get_option('facebook_share_where') == 'beforeandafter') echo 'selected="selected"'; ?> value="beforeandafter">Before and After</option>

							<option <?php if (get_option('facebook_share_where') == 'manual') echo 'selected="selected"'; ?> value="manual">Manual</option>

                		</select><br>
<p>
If you use Facebook Share button it like on kumsaati.org then use<b> clear:left; float: left; margin-right: 10px; margin-top:10px;</b> </p>

                    <input name="facebook_share_style" type="text" id="facebook_share_style" value="<?php echo htmlspecialchars(get_option('facebook_share_style')); ?>" size="30" />
                  

		<br><br>
            <input type="submit" name="Submit" value="<?php _e('Save Changes') ?>" />
    </form>
		</div>
<?php } ?>
