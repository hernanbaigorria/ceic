=== Plugin Name ===
Contributors: kumsaati
Donate link: http://kumsaati.org/
Tags: facebook, facebook share button
Requires at least: 2.0.2
Tested up to: 2.9.2
Stable tag: 1.0

The Facebook Share button easily allows your blog to be shared.

== Description ==

The Facebook Share button easily allows your blog to be shared.

Tested upto 2.9.2

== Installation ==

Follow the steps below to install the plugin.

   1. Upload the facebook share directory to the /wp-content/plugins/ directory
   2. Activate the plugin through the 'Plugins' menu in WordPress
   3. Go to "Facebook Share" option to configure the button

== Frequently Asked Questions ==

coming soon



== Screenshots ==

coming soon

== Changelog ==
coming soon

== Upgrade Notice ==
coming soon

`<?php code(); // goes in backticks ?>`
