<?php get_header(); ?>




  <div id="cuerpo-contenido">

  <div id="cuerpo">
	 <div id="salida-nota">

          <?php if (have_posts()) : ?>

 	  <?php $post = $posts[0]; // Hack. Set $post so that the_date() works. ?>
 	  <?php /* If this is a category archive */ if (is_category()) { ?>
		<h4 ><?php single_cat_title(); ?></h4>
 	  <?php /* If this is a tag archive */ } elseif( is_tag() ) { ?>
		<h2 class="pagetitle">Posts Tagged &#8216;<?php single_tag_title(); ?>&#8217;</h2>
 	  <?php /* If this is a daily archive */ } elseif (is_day()) { ?>
		<h2 class="pagetitle">Archive for <?php the_time('F jS, Y'); ?></h2>
 	  <?php /* If this is a monthly archive */ } elseif (is_month()) { ?>
		<h2 class="pagetitle">Archive for <?php the_time('F, Y'); ?></h2>
 	  <?php /* If this is a yearly archive */ } elseif (is_year()) { ?>
		<h2 class="pagetitle">Archive for <?php the_time('Y'); ?></h2>
	  <?php /* If this is an author archive */ } elseif (is_author()) { ?>
		<h2 class="pagetitle">Author Archive</h2>
 	  <?php /* If this is a paged archive */ } elseif (isset($_GET['paged']) && !empty($_GET['paged'])) { ?>
		<h2 class="pagetitle">Blog Archives</h2>
 	  <?php } ?>

       
	<?php while (have_posts()) : the_post(); ?>

        <a href="<?php the_permalink() ?>" rel="bookmark">
            <div class="box-archivo">
                <div class="box-archivoImg">
                    <?php mi_imagen($tamano = 'medio') ; ?>
                </div>
                <div class="box-archivoTxt">
                      <h6><?php $key="autor"; echo get_post_meta($post->ID, $key, true); ?></h6>
                      <h3><?php the_title(); ?></h3>                                          
                      <?php the_content('') ?>
                </div>
            </div>
        </a>



        <?php endwhile; ?>
        <div style="margin:20px 10px; float:left;">
            <?php if(function_exists('wp_paginate')) {
                wp_paginate();
            } ?>
            </div>
        </div>

	<?php else :

		if ( is_category() ) { // If this is a category archive
			printf("<h2 class='center'>Esta secci&oacute;n esta en construcci&oacute;n</h2>", single_cat_title('',false));
		} else if ( is_date() ) { // If this is a date archive
			echo("<h2>Esta secci&oacute;n esta en construcci&oacute;n</h2>");
		} else if ( is_author() ) { // If this is a category archive
			$userdata = get_userdatabylogin(get_query_var('author_name'));
			printf("<h2 class='center'>Esta secci&oacute;n esta en construcci&oacute;n</h2>", $userdata->display_name);
		} else {
			echo("<h2 class='center'>No hay contenido.</h2>");
		}
		?><h1><center><img src="<?php bloginfo('template_url'); ?>/imagenes/isologo.png">

                <?php get_search_form('Buscar'); ?> </h1></center>   <?php

	endif;
    ?>


    



                    </div>


<?php get_footer(); ?>

