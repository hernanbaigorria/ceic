If you are upgrading from FREE to PRO version follow any one of the steps below.

1. Upload the PRO version directly into the plugins directory. Deactivate the FREE version and activate the PRO.

2. Completely remove the FREE version and upload the PRO version directly into the plugins directory, and activate it.


In both the cases, all your previously added content (banners/images) will be retained.