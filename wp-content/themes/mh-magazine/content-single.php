<style type="text/css">.mh-breadcrumb{display:none!important;}</style>
<?php /* Default template for displaying post content */
$mh_magazine_options = mh_magazine_theme_options(); ?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="row">
		<div class="col-xs-12 col-sm-1" style="max-width: 5%;"></div>
		<div class="col-xs-12 col-sm-6">
		<header class="entry-header clearfix">
			<?php
			echo '<ul class="header-nav-custom">';
			mh_post_header_custom();
			echo '</ul>';
			//if (!is_home() && !is_front_page()) {
				//global $post;
				//$mh_magazine_options = mh_magazine_theme_options();
				//if ($mh_magazine_options['breadcrumbs'] == 'enable') {
				//
				//	if (is_single() && get_post_type() == 'post' && !is_attachment()) {
				//		$category = get_the_category();
				//		$category_id = $category[0]->cat_ID;
				//		$parent_id = $category[0]->category_parent;
				//		$parents = get_category_parents($parent_id, true, $delimiter);
				//		if ($parent_id != 0) {
				//			echo $parents;
				//		}
				//		if (esc_attr($category[0]->name) == 'Análisis') {
				//			echo '<style type="text/css">.entry-title {color:#89363d;}</style>';
				//		}
				//	} 
					
				//}
			//}
			the_title('<h1 class="entry-title">', '</h1>');
			?>
		</header>
		</div>
		<div class="col-xs-12 col-sm-1"></div>
		<div class="col-xs-12 col-sm-4"></div>
			<div class="col-xs-12 col-sm-1" style="max-width: 5%;"></div>
			<div class="col-xs-12 col-sm-10" style="padding:0;">
			<hr class="border-post">
			</div>
			<div class="col-xs-12 col-sm-1"></div>
		
		
	</div>
	<?php dynamic_sidebar('mh-posts-1'); ?>
	<div class="row" style="position:relative;">
		<div class="col-xs-12 col-sm-1" style="max-width: 5%;"></div>
		<div class="col-xs-12 col-sm-6">
			<div class="theme">
				<?php 
				if (!is_home() && !is_front_page()) {
					global $post;
					$mh_magazine_options = mh_magazine_theme_options();
					if ($mh_magazine_options['breadcrumbs'] == 'enable') {
					
						if (is_single() && get_post_type() == 'post' && !is_attachment()) {
							$category = get_the_category();
							$category_id = $category[0]->cat_ID;
							$parent_id = $category[0]->category_parent;
							$parents = get_category_parents($parent_id, true, $delimiter);
							if ($parent_id != 0) {
								echo $parents;
							}
							if (esc_attr($category[0]->name) == 'Competitividad y Desarrollo Local') {
								echo '<style type="text/css">.theme {background: #89363d;}</style>';
								echo esc_attr($category[0]->name);
							}

						} 
						
					}
				} ?>
			</div>
			<div class="entry-content clearfix"><?php
				//mh_post_content_top();
				the_content();
				//mh_post_content_bottom(); ?>
				<div class="box-autor">
					<div class="row">
						<div class="col-xs-12 col-sm-2">
							<div style="background:url('<?php echo get_wp_user_avatar_src(get_the_author_meta("ID")); ?>');padding-bottom: 100%;background-repeat: no-repeat;background-size: contain;max-width:100%;"></div>
						</div>
						
						<div class="col-xs-12 col-sm-10" style="padding-left:0;">
							<div>
								<h3><?php the_author_meta('first_name')?> <?php the_author_meta('last_name'); ?></h3>
							</div>
							<div class="enlaces">
								<?php if (get_the_author_meta('facebook')):?>
								<a href="<?php the_author_meta('facebook'); ?>" class="red-social">
									<i class="fa fa-facebook"></i>
								</a>
								<?php endif ?>

								<?php if (get_the_author_meta('twitter')):?>
								<a href="<?php  the_author_meta('twitter'); ?>" class="red-social">
									<i class="fa fa-twitter"></i>
								</a>
								<?php endif ?>
		
								<?php if (get_the_author_meta('linkedin')):?>
								<a href="the_author_meta('linkedin');" class="red-social">
									<i class="fa fa-linkedin-in"></i>
								</a>
								<?php endif ?>
							</div>
							<div class="web">
								<?php if(get_the_author_meta('user_url')): ?>
										/ <a href="<php the_author_meta('user_url') ?>"> <?php the_author_meta('user_url') ?></a>
								<?php endif ?>
							</div>
							<div class="col-12 desc-aut">
								<?php if(get_the_author_meta('user_description')): ?>
									<p><?php the_author_meta('user_description') ?></p>
								<?php endif ?>
							</div>
						</div>
					</div>
				</div>
				<div class="col-12 revista padding-0">
					<?php

						$titulo = get_field('titulo');?>

							<?php if (!empty($titulo)): ?>
								<h3><?php echo $titulo ?></h3>
							<?php endif ?>
						<?php
						// check if the repeater field has rows of data
						if( have_rows('detalle') ):

						 	// loop through the rows of data
						    while ( have_rows('detalle') ) : the_row();
						    	$subtitulo = get_sub_field('subtitulo');?>
						    	<?php if (!empty($subtitulo)): ?>
						        	<h2><?php echo $subtitulo ?></h2>
						        <?php endif ?>

						        <?php 
						           while ( have_rows('elementos_de_descripcion') ) : the_row();
						           	$descripcion = get_sub_field('descripcion');
						           	$autor = get_sub_field('autor');
						           	$pdf = get_sub_field('pdf');?>

						        	<div class="col-12 padding-0">
						        		<p><?php echo $descripcion ?></p>
						        	</div>
						        	<div class="col-12 padding-0">
						        		<?php if (!empty($autor)): ?>
						        			<h6><?php echo $autor ?> /</h6>
						        		<?php endif ?>
						        		<? if( $pdf ): ?>
						        			
						        			<a href="<?php echo $pdf['url']; ?>">Descargar</a>

						        		<?php endif; ?>
						        	</div>

						           <? endwhile;

						    endwhile;

						endif; 
					?>				
				</div>

				<div class="col-12 documentos_de_trabajo padding-0">
					<?php
						if( have_rows('documentos_de_trabajo') ):?>
								<?
							 	// loop through the rows of data
							    while ( have_rows('documentos_de_trabajo') ) : the_row();
							    	$pdf = get_sub_field('pdf');?>
									<div class="row d-flex">
								    	<div class="col-xs-12 col-sm-7 border-r">
								          <? while ( have_rows('detalles') ) : the_row();
									           	$titulo = get_sub_field('titulo');
									           	$descripcion = get_sub_field('descripcion');
									           	?>

									        	<div class="margin-d">
									        		<h3><?php echo $titulo ?></h3>
									        		<p><?php echo $descripcion ?></p>
									        	</div>

								           <? endwhile; ?>
							           	</div>

							           	<div class="col-xs-12 col-sm-5" style="margin:auto 0;">

								           <? if( $pdf ): ?>
								        			
								        			<a href="<?php echo $pdf['url']; ?>" target="_blank"><i class="far fa-file-pdf"></i> Descargar</a>

								        	<? endif; ?>
							        	</div>
							    	</div>
							    <? endwhile;?>
						<? endif; 
					?>				
				</div>
			</div>
			<!--<?php
			if ($mh_magazine_options['tags'] === 'enable') {
				the_tags('<div class="entry-tags clearfix"><i class="fa fa-tag"></i><ul><li>','</li><li>','</li></ul></div>');
			}
			dynamic_sidebar('mh-posts-2'); ?>-->
		</div>
		<div class="col-xs-12 col-sm-1"></div>
		<div class="col-xs-12 col-sm-4 relation-notes">
			<h6>NOTAS RELACIONADAS</h6>
			<?php mh_after_post_content(); ?>
			<div class="d-flex banner-lateral-post flex-column">
				<h3>¿QUERÉS RECIBIR EL LIBRO
					ANUAL DE PUBLICACIONES?</h3>
				<p>Completá tus datos y subscribite
					para ser parte de la comunidad.</p>
				<a href="#" class="btn-suscr-post">Subscribirme</a>
			</div>
		</div>
	</div>
</article>

