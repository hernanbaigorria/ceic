<?php
/**
 * The template for displaying search results pages
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header(); ?>

	<div class="container container-main">
		<div class="row">

		<?php if ( have_posts() ) : ?>
			<header class="page-header col-xs-12">
				<div class="row">
					<div class="col-md-10 col-md-offset-1 archive-title col-xs-12">
						<h1 class="page-title"><?php printf( 'Resultados de busqueda para: %s', '<span>' . esc_html( get_search_query() ) . '</span>' ); ?></h1>
					</div>
				</div>
			</header><!-- .page-header -->
			<div class="row">
				<div class="col-md-8 col-md-offset-2">
					<hr />
				</div>
			</div>

			<?php
			// Start the loop.
			while ( have_posts() ) : the_post();

				/**
				 * Run the loop for the search to output the results.
				 * If you want to overload this in a child theme then include a file
				 * called content-search.php and that will be used instead.
				 */
				get_template_part( 'template-parts/content', 'content' );

			// End the loop.
			endwhile;
			 ?>
			<div class="row">
				<div class="col-md-8 col-md-offset-2 text-center">
					<hr />
					<?php 
					the_posts_pagination( array(
						'mid_size'			 => 5,
						'prev_text'          => '<i class="fa fa-arrow-left" aria-hidden="true"></i>',
						'next_text'          => '<i class="fa fa-arrow-right" aria-hidden="true"></i>',
						'before_page_number' => '<span class="meta-nav screen-reader-text"> </span>',
						'screen_reader_text' => ' '
					) );
					?>
				</div>
			</div>

		<?php
			// If no content, include the "No posts found" template.
			else :
				get_template_part( 'template-parts/content', 'none' );

			endif;
			?>

		</div>
	</div>
<?php get_footer(); ?>
