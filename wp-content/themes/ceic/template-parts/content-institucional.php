<?php
/**
 * The template part for displaying content
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
$classes = array('col-md-4', 'home-box');
?>

<div class="item">
	<div class="row">
	  <div class="col-xs-5">
		<?php 
		$miniaturaPersonalizada = CFS()->get( 'imagen_en_miniatura_para_home' );
		if(isset($miniaturaPersonalizada) && !empty($miniaturaPersonalizada)){
			echo wp_get_attachment_image( $miniaturaPersonalizada, 'institucional-size', false, array( 'class' => 'img-responsive bg' ) );
		}else{
			the_post_thumbnail('institucional-size', array( 'class' => 'img-responsive bg' ));
		}?>
	  </div>
	  <div class="col-xs-7" style="height: 145px;">
	  	<?php the_title( sprintf( '<h2><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>
	    <p class="author">-<br><?php $key="autor"; $author = get_post_meta($post->ID, $key, true); echo ($author != '')?$author:'CEIC'; ?></p>
	  </div>
	</div>
</div>
