<form role="search" method="get" class="search-form nav navbar-nav" action="<?php echo esc_url( home_url( '/' ) ); ?>">
	<label>
		<input type="search" class="search-field" value="<?php echo get_search_query(); ?>" name="s" />
	</label>
	<button type="submit" class="search-submit"><span class="screen-reader-text"><i class="fa fa-search" aria-hidden="true"></i></span></button>
</form>
