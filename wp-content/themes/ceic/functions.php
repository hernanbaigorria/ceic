<?php

function ceic_setup() {


	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for custom logo.
	 *
	 *  @since Twenty Sixteen 1.2
	 */
	add_theme_support( 'custom-logo', array(
		'width'       => 100,
		'height'      => 75,
		'flex-height' => true,
	) );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	add_theme_support( 'post-thumbnails' );
	set_post_thumbnail_size( 1200, 9999 );
	add_image_size( 'home-size', 420, 380, true );
	add_image_size( 'archive-size', 230, 189, true );
	add_image_size( 'institucional-size', 150, 150 );
	add_image_size( 'slider-size', 1159, 580, true );
	add_image_size( 'post-size', 999999, 475, true );
	add_image_size( 'destacadas-size', 370, 147, true );

	// This theme uses wp_nav_menu() in two locations.
	register_nav_menus( array(
		'superior' => 'Menu Superior',
		'primary' => 'Menu Principal',
		'footer'  => 'Menu Footer',
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 *
	 * See: https://codex.wordpress.org/Post_Formats
	 */
	add_theme_support( 'post-formats', array(
		'aside',
		'image',
		'video',
		'quote',
		'link',
		'gallery',
		'status',
		'audio',
		'chat',
	) );

	// Indicate widget sidebars can use selective refresh in the Customizer.
	add_theme_support( 'customize-selective-refresh-widgets' );
}
add_action( 'after_setup_theme', 'ceic_setup' );


/**
 * Registers a widget area.
 */
function ceic_widgets_init() {
	register_sidebar( array(
		'name'          => 'Footer 1',
		'id'            => 'footer-1',
		'description'   => 'Agregar widgets',
		'before_widget' => '',
		'after_widget'  => '',
		'before_title'  => '<h2 class="hide">',
		'after_title'   => '</h2>',
	) );

	register_sidebar( array(
		'name'          => 'Footer 2',
		'id'            => 'footer-2',
		'description'   => 'Agregar widgets',
		'before_widget' => '',
		'after_widget'  => '',
		'before_title'  => '<h2 class="hide">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'ceic_widgets_init' );

function ceic_logo() {
   if ( function_exists( 'the_custom_logo' ) ) {
      the_custom_logo();
   }
}

function ceic_enqueue_scripts() {
    wp_enqueue_style( 'style', get_stylesheet_uri() );
    wp_enqueue_style( 'font-awesome', '//maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css', array(), '4.6.3' );
	wp_enqueue_style( 'owl-carousel', get_template_directory_uri().'/vendor/owl-carousel/assets/owl.carousel.css', array(), '4.6.3' );

	wp_enqueue_script( 'owl-carousel-js', get_template_directory_uri(). '/vendor/owl-carousel/owl.carousel.min.js', array( 'jquery' ), '', true );
}
add_action( 'wp_enqueue_scripts', 'ceic_enqueue_scripts' );

function ceic_categories($separator = ' ') {
	$categories = get_the_category();
    $output = '';
    if($categories){
    	$output .= '<span class="cat-links">';
        foreach($categories as $category) {
        	$rl_category_color = rl_color($category->cat_ID);
            $output .= '<div class="cat-link"><div class="cuadrado" style="background:'.$rl_category_color.';"></div><a href="'.get_category_link( $category->term_id ).'" rel="category tag" style="color:'.$rl_category_color.';">'.$category->cat_name.'</a></div>'.$separator;
        }
        $output .= '</span>';
    echo trim($output, $separator);
    }
}

/**
 * Filter the except length to 20 characters.
 *
 * @param int $length Excerpt length.
 * @return int (Maybe) modified excerpt length.
 */
function wpdocs_custom_excerpt_length( $length ) {
    return 35;
}
add_filter( 'excerpt_length', 'wpdocs_custom_excerpt_length', 1 );

add_filter( 'get_the_archive_title', function ($title) {

    if ( is_category() ) {

            $title = single_cat_title( '', false );

        } elseif ( is_tag() ) {

            $title = single_tag_title( '', false );

        } elseif ( is_author() ) {

            $title = '<span class="vcard">' . get_the_author() . '</span>' ;

        }

    return $title;

});