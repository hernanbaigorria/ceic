<?php
$classes = "col-xs-12";
?>
<article id="post-<?php the_ID(); ?>" <?php post_class($classes); ?>>
  <header class="entry-header">
    <?php $mostrarEncabezado = CFS()->get('encabezado_con_imagen'); ?>
    <div class="row <?php echo ($mostrarEncabezado)?'bg-title':'';?>">
      <div class="col-md-8 col-md-offset-2">
        <?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
      </div>
    </div>
    <?php
if (has_post_thumbnail() ) { ?>
      <div class="row">
        <div class="col-md-10 col-md-offset-1">
          <?php the_post_thumbnail('post-thumbnail', array( 'class' => 'img-responsive' )); ?>
        </div>
      </div>
      <?php } ?>
  </header>
  <!-- .entry-header -->

  <div class="entry-content">
    <div class="row">
      <div class="col-md-8 col-md-offset-2">
        <?php
        the_content();
        ?>
      </div>
    </div>
    <?php
$fields = CFS()->get( 'memorias' );
foreach ( $fields as $field ) {?>

      <div class="row">
        <div class="col-md-8 col-md-offset-2 memorias-links">
          <a href="<?php echo $field['archivo']; ?>">
            <?php echo $field['titulo']; ?>
          </a>
        </div>
      </div>

      <?php } ?>
  </div>
  <!-- .entry-content -->
</article>
<!-- #post-## -->