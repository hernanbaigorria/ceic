<?php
/**
 * The template for displaying archive pages
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * If you'd like to further customize these archive views, you may create a
 * new template file for each one. For example, tag.php (Tag archives),
 * category.php (Category archives), author.php (Author archives), etc.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header(); ?>

	<div class="container container-main">
		<div class="row">

		<?php if ( have_posts() ) : ?>

			<header class="page-header">
				<div class="row">
					<div class="col-md-10 col-md-offset-1 col-xs-12 archive-title">
						<div class="col-md-10 col-md-offset-1 col-xs-10">
  							<h1><?php echo get_the_archive_title();?></h1>
  						</div>
  						<div class="col-md-1 col-xs-1 text-center" style="color: #fff;padding-top: 5px;padding-bottom: 5px;line-height: 32px;cursor: pointer;">
  							<i class="fa fa-plus expand-contract-click" aria-hidden="true" id="expand"></i>
  							<i class="fa fa-minus contract expand-contract-click" aria-hidden="true" id="contract"></i>
  						</div>
					</div>
				</div>
				<div class="row expand-contract">
					<div class="col-md-8 col-md-offset-2 col-xs-12">
						<?php the_archive_description( '<div class="taxonomy-description">', '</div>' ); ?>
					</div>
				</div>
			</header><!-- .page-header -->
			<div class="row expand-contract">
				<div class="col-md-8 col-md-offset-2">
					<hr />
				</div>
			</div>


			<?php
			// Start the Loop.
			while ( have_posts() ) : the_post();
				/*
				 * Include the Post-Format-specific template for the content.
				 * If you want to override this in a child theme, then include a file
				 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
				 */
				get_template_part( 'template-parts/content', get_post_format() );

			// End the loop.
			endwhile; ?>
			<div class="row">
				<div class="col-md-8 col-md-offset-2 text-center">
					<hr />
					<?php 
					the_posts_pagination( array(
						'mid_size'			 => 5,
						'prev_text'          => '<i class="fa fa-arrow-left" aria-hidden="true"></i>',
						'next_text'          => '<i class="fa fa-arrow-right" aria-hidden="true"></i>',
						'before_page_number' => '<span class="meta-nav screen-reader-text"> </span>',
						'screen_reader_text' => ' '
					) );
					?>
				</div>
			</div>
			<?php
			// Previous/next page navigation.
			

		// If no content, include the "No posts found" template.
		else :
			get_template_part( 'template-parts/content', 'none' );

		endif;
		?>

		</div>
	</div>
<?php get_footer(); ?>
