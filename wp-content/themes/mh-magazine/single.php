<?php get_header(); ?>
<div class="mh-wrapper clearfix">
	<div class="mh-main clearfix">
		<div id="main-content" class="container" role="main" itemprop="mainContentOfPage"><?php
			while (have_posts()) : the_post();?>
			<div class="row">
				<div class="col-xs-12 col-sm-12">
				<?php
				mh_before_post_content();
				get_template_part('content', 'single');?>
				</div>

				<?php comments_template();?>
			</div>
			<?php endwhile; ?>
		</div>
		<!--<?php get_sidebar(); ?>-->
	</div>
    <?php  mh_magazine_second_sidebar(); ?>
</div>
<?php get_footer(); ?>