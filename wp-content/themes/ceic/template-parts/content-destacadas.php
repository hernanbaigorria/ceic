<?php
/**
 * The template part for displaying content
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
$classes = array('col-md-4', 'destacadas-home', 'n-p-l'); ?>

<article id="post-<?php the_ID(); ?>" <?php post_class($classes); ?> onclick="location.href='<?php echo esc_url( get_permalink() ); ?>'">
		<?php the_post_thumbnail('destacadas-size', array( 'class' => 'img-responsive bg' )); ?>
        <?php the_title( sprintf( '<h2><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>
		<div class="over"></div>
</article><!-- #post-## -->
