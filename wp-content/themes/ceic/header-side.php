<div class="container-fluid">
	<div class="row">
		<div class="col-lg-2 col-md-1 menu-lateral" id="hola">
			<?php if ( has_nav_menu( 'primary' ) ) : ?>
				<nav id="side-navigation" class="side-navigation" role="navigation" aria-label="Side Links Menu">
					<?php wp_nav_menu( array( 
						  	'theme_location' 	=> 'primary',
						  	'container' 		=> '',
						  	'menu_class'      	=> 'nav',
						  	'menu_id'         	=> '',
						  	'echo'            	=> true,
						  	'fallback_cb'     	=> 'wp_page_menu',
						  	'before'          	=> '',
						  	'after'           	=> '',
						  	'link_before'     	=> '',
						  	'link_after'      	=> '',
						  	'items_wrap'      	=> '<ul id="%1$s" class="%2$s">%3$s</ul>',
						  	'depth'           	=> 0,
						  	'walker'          	=> ''
					)); ?>

					<ul id="menu-footer" class="nav">
						<li>
							<a href="http://www.facebook.com/pages/Grupo-de-Estudios-Internacionales-Contempor%C3%A1neos-GEIC/188026151274947">
							<i class="fa fa-facebook" aria-hidden="true"></i></a>
						</li>
						<li>
							<a href="https://twitter.com/CEIC_">
							<i class="fa fa-twitter" aria-hidden="true"></i></a>
						</li>
						<li>
							<a href="https://www.linkedin.com/company/10780628?trk=tyah&trkInfo=clickedVertical%3Acompany%2CclickedEntityId%3A10780628%2Cidx%3A2-1-2%2CtarId%3A1475173977738%2Ctas%3Acentro%20de%20estudios%20internacionales%20con">
							<i class="fa fa-linkedin" aria-hidden="true"></i></a>
						</li>
					</ul>
					<ul class="nav">
						<li>
							<img src="<?php echo get_template_directory_uri().'/img/menu-cerrar.png'; ?>" class="navbar-toggle" style="float: left;margin-top: 30px;cursor: pointer;"/>
						</li>
					</ul>
				</nav><!-- .social-navigation -->
			<?php endif; ?>
		</div>
	</div>
</div>