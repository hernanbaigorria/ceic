<?php get_header(); ?>

	<div id="cuerpo-contenido">
		
        <div id="cuerpo">

       	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		        
        <div id="salida-nota">
			
            
            <h4><?php the_time(__('j/m/y')); ?> - <?php $key="subtitulo"; echo get_post_meta($post->ID, $key, true); ?></h4>
			<h6><?php $key="autor"; echo get_post_meta($post->ID, $key, true); ?></h6>
            <h1><?php the_title(); ?></h1>
                     
            <?php global $more; $more = FALSE; ?>
            
            <strong><?php the_content(''); ?></strong>
            
            	<div class="salidaBoxFoto">
                    <div class="imgBoxFoto"><?php mi_imagen($tamano = 'grande') ; ?></div>
                    <div class="bajadaBoxFoto"><?php $key="bajada-foto"; echo get_post_meta($post->ID, $key, true); ?></div>
                </div>
                
			<?php $more = TRUE; ?>
			
            <p> <?php the_content(0,1,''); ?></p>
            
            
            
			<?php $numeronopubli = get_the_ID(); ?>
            
			<div style="border-bottom: 1px dotted #333333;  border-top: 1px dotted #333333; float: left; margin: 40px 0 20px; padding: 10px 0 2px; width: 620px;;" class="compartir">
            
			<div class="lefts">
            	<iframe src="http://www.facebook.com/plugins/like.php?href=<?php the_permalink(); ?>&amp;layout=button_count&amp;show_faces=false&amp;&amp;action=like&amp;font=arial&amp;colorscheme=light" scrolling="no" frameborder="0" style="border:none; overflow:hidden; height:30px; width:110px"></iframe>
            </div> 
                                
                                
  <div class="lefts">
    <script src="http://platform.twitter.com/widgets.js" type="text/javascript"></script>
    <a href="http://twitter.com/share" class="twitter-share-button"
    data-url="<?php the_permalink(); ?>"
    data-via="USUARIO DE TWITTER"
    data-text="<?php the_title(); ?>"
    data-count="horizontal"
    data-lang="es">Tweet</a>
 </div>

<div class="lefts">
<!-- Place this tag where you want the +1 button to render Coloque esta etiqueta donde desea que el botón para hacer una  -->
<g:plusone size="medium"></g:plusone>

<!-- Place this render call where appropriate Este lugar hacen llamar en su caso  -->
<script type="text/javascript">
  window.___gcfg = {lang: 'es'};

  (function() {
    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
    po.src = 'https://apis.google.com/js/plusone.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
  })();
</script>
</div>

 <div class="lefts" style="padding-right:10px;">
   <script type="text/javascript" src="http://platform.linkedin.com/in.js"></script><script type="in/share" data-url="<?php the_permalink(); ?>" data-counter="right"></script></div>
 
 

<div class="clearleft"></div>
 
</div><!-- Fin Compartir -->




            
<div style="margin:20px 0 0 0;" id="fb-root"><script src="http://connect.facebook.net/es_LA/all.js#xfbml=1"></script>
            <fb:comments href="<?php echo get_permalink(); ?>" num_posts="45" width="620"></fb:comments></div>
            

             
            <div style="margin:-10px 0 0 0;">
            <h4>Te Puede Interesar</h4>
            <?php

$etiquetas = wp_get_post_tags($post->ID); // Obtenemos las etiquetas del articulo actual

if ($etiquetas) {

	$tag_ids = array(); // Creamos un Array vacio para almacenar las ID de las etiquetas

	foreach($etiquetas as $etiqueta) {

		$etiquetas_id[] = $etiqueta->term_id; // Obtenemos la ID de cada etiqueta y las guardamos en el array etiquetas_id[]
	
	}

	$parametros = array(
		'post__not_in' => array($post->ID), // No mostramos el articulo actual
		'tag__in' => $etiquetas_id,  // Le indicamos que etiquetas comparar
		'showposts'=>5,  // Numero de articulos a mostrar
	);

	$resultados = new wp_query($parametros); // Realizamos la query con los parametros anteriores

	if( $resultados->have_posts() ) {  // Si hay resultados
		while ($resultados->have_posts()) {  // Empezamos un bucle para mostrar los articulos
			$resultados->the_post(); // Inicializamos el post para usarlo en el loop
			// Mostramos los articulos relacionados
		?>
			<div class="relacionado">
			<a href="<?php the_permalink() ?>" title="<?php the_title(); ?>">
				<span class="texto"><?php the_title(); ?></span>
			</a>
			</div>
		<?php
		}
	} else { // Si no hay resultados
		?> <p>No hay articulos relacionados </p> <?php
	}
	wp_reset_postdata(); // Reinicializamos los datos del post original por 
					     //si queremos mostrar mas datos despues de los articulos relacionados
}

?>
            
            
            
            
           <!-- <?php $ID = $wp_query->posts[0]->ID;
$tagsvariable = get_the_tags($ID);
$query= 'cat=' . $tagsvariable. '&orderby=date&order=ASC';
query_posts($query);

?>
<h2>Noticias Relacionadas</h2>
<ul style="list-style:none; padding:0px;">
<?php if (have_posts()) : ?>
<?php while (have_posts()) : the_post(); ?>
<li style="padding:0px;">
<a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><br />
<?php the_title(); ?>
</a>
</li>
<?php endwhile; endif; wp_reset_query(); ?>
</ul>-->
            
            

            
            </div>
            
            
        <?php /*?><?php comments_template( '', true ); ?><?php */?>
        </div>
        	<?php endwhile; else: ?>

                <p>Sorry, no posts matched your criteria.</p>

          <?php endif; ?>













       
       
       
     <!-- <div class="redes-sociales">
         <?php quailpress_share(); ?> - <?php if(function_exists('the_views')) { the_views(); } ?>
       </div>-->


                </div>


<?php get_footer(); ?>
