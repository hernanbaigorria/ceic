<?php
$classes = "col-xs-12";
?>
<article id="post-<?php the_ID(); ?>" <?php post_class($classes); ?>>
	<header class="entry-header">
		<?php $mostrarEncabezado = CFS()->get('encabezado_con_imagen'); ?>
		<div class="row <?php echo ($mostrarEncabezado)?'bg-title':'';?>">
			<div class="col-md-8 col-md-offset-2">
				<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
			</div>
		</div>
		
		<?php 
		if (has_post_thumbnail() ) { ?>
			<div class="row">
				<div class="col-md-10 col-md-offset-1">
			<?php the_post_thumbnail('post-thumbnail', array( 'class' => 'img-responsive' )); ?>
				</div>
			</div>
		<?php } ?>

	</header><!-- .entry-header -->

	<div class="entry-content">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
		<?php
		the_content();

		wp_link_pages( array(
			'before'      => '<div class="page-links"><span class="page-links-title">Paginas:</span>',
			'after'       => '</div>',
			'link_before' => '<span>',
			'link_after'  => '</span>',
			'pagelink'    => '<span class="screen-reader-text">Pagina </span>%',
			'separator'   => '<span class="screen-reader-text">, </span>',
		) );
		?>
			</div>
		</div>
	</div><!-- .entry-content -->

	<?php
		edit_post_link(
			sprintf(
				'Editar<span class="screen-reader-text"> "%s"</span>',
				get_the_title()
			),
			'<footer class="entry-footer"><span class="edit-link">',
			'</span></footer><!-- .entry-footer -->'
		);
	?>

</article><!-- #post-## -->
