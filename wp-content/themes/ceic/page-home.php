<?php
/*
Template Name: Homepage
*/
get_header('home'); ?>

<div class="container container-main">
	<div class="row links-home">
		<div class="col-md-4"><a href="<?php echo get_category_link(10);?>" rel="category tag"><?php echo get_cat_name(10); ?></a></div>
		<div class="col-md-4"><a href="<?php echo get_category_link(23);?>" rel="category tag"><?php echo get_cat_name(23); ?></a></div>
		<div class="col-md-4"><a href="<?php echo get_category_link(12);?>" rel="category tag"><?php echo get_cat_name(12); ?></a></div>
	</div>
	<div class="row">
		<?php
		// Start the loop.
		query_posts('posts_per_page=9&cat=15,6,9,11');

		while ( have_posts() ) : the_post();

			// Include the page content template.
			get_template_part( 'template-parts/content', 'home' );

			// End of the loop.
		endwhile;
		?>
		<?php wp_reset_query(); ?>
	</div>
	<div class="row hidden-xs">
		<div class="col-md-12 n-p"><h1 class="institucional-title"><a href="<?php echo get_category_link(18);?>">NOTICIAS</a></h1></div>
	</div>

	<div class="row hidden-xs">

		<div id="owl-institucional">
			<?php
			// Start the loop.
			query_posts('posts_per_page=-1&cat=18');

			while ( have_posts() ) : the_post();

				// Include the page content template.
				get_template_part( 'template-parts/content', 'institucional' );

				// End of the loop.
			endwhile; ?>
			<?php wp_reset_query(); ?>
		</div>
	</div>

	<div class="row row-destacadas hidden-xs">
		<div class="col-md-12 n-p"><h1 class="institucional-title"><a href="<?php echo get_category_link(654);?>">PROGRAMAS</a></h1></div>
	</div>

	<div class="row hidden-xs">
		<?php
		// Start the loop.
		query_posts('posts_per_page=6&cat=654');

		while ( have_posts() ) : the_post();

			// Include the page content template.
			get_template_part( 'template-parts/content', 'destacadas' );

			// End of the loop.
		endwhile;
		?>
		<?php wp_reset_query(); ?>
	</div>

</div>

<?php get_footer(); ?>
