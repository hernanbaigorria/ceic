<?php

function mi_imagen($tamano = '') {
  if ( $images = get_children( array (
    'post_parent'    => get_the_ID(),
    'post_type'      => 'attachment',
    'numberposts'    => 1,
    'post_mime_type' => 'image'
   )));
  {
    if(!empty($images)) { //Solo a�ad� esta l�nea
        foreach( $images as $image ) {
          if($tamano == 'mini') {
          $imagen = wp_get_attachment_image( $image->ID, 'thumbnail' );
          } if($tamano == 'medio') {
          $imagen = wp_get_attachment_image( $image->ID, 'medium' );
          } if($tamano == 'grande') {
          $imagen = wp_get_attachment_image( $image->ID, 'full' );
          }
          echo $imagen;
        }     
    } // y su respectivo cierre

  }
}
if ( function_exists('register_sidebar') )
register_sidebar(array(
'name' => 'Sidebar',
'before_widget' => '<div class="sidebaritem">',
'after_widget' => '</div>',
'before_title' => '<h3>',
'after_title' => '</h3>',
));

function personalizar_login() {
    echo '<style type="text/css">
          h1 a { background-image:url('.get_bloginfo('template_directory').'/imagenes/logo.jpg) !important; }
    </style>';
}
add_action('login_head', 'personalizar_login');

// Add default posts and comments RSS feed links to head
add_theme_support( 'automatic-feed-links' );