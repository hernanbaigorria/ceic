=== Responsive Styled Google Maps Simplified ===
Contributors: greenlline
Tags: address, map, responsive map, styled map, google map, responsive styled map, contact map, easy map, quick map, mobile map
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html
Requires at least: 4.5
Tested up to: 4.6.1
Stable tag: trunk
Donate link: http://codecanyon.net/item/responsive-styled-google-maps-wordpress-plugin/3909576?ref=greenline

Easily display a responsive Google Map (classic map or black&white map) using an incredibly easy shortcode. 

== Description ==

If you need to quickly set up a responsive (100% width, mobile friendly) map, this is a solution for you. The map can be a classic, colored map or a black-white one. You can use the shortcode in a page, in a post, in the footer of your website or in a sidebar widget.

Please note that a Google Maps API key is required now by Google, since 22 June 2016, according to their official post: https://googlegeodevelopers.blogspot.com/2016/06/building-for-scale-updates-to-google.html

**Shortcode examples**

Classic map:

[resmap address="New York" style="1" zoom="10" height="300px" key="your_google_maps_api_key_here"]

Black&white map:

[resmap address="Eiffel Tower, France" style="2" zoom="14" height="500px" key="your_google_maps_api_key_here"]

Using latitude, longitude:

[resmap address="40.71297887582146, -74.00594130000002" style="1" zoom="10" height="300px" key="your_google_maps_api_key_here"]


**Shortcode parameters**

* address - the address you wish to show (an address or latitude, longitude)
* style - "1" for a classic map or "2" for a black&white map
* zoom - a number between 1-19
* height - in pixels, for instance 300px, or 400px
* key - your Google Maps API key obtained from: https://developers.google.com/maps/documentation/javascript/get-api-key

**Simplified version (this one, available here):**

* Google maps plugin to generate maps based on address
* 100% responsive and mobile friendly
* Works in sidebars, posts, pages and custom post types
* There can be more than one maps in a page/post
* A Google Maps API key is required since 22 June 2016 ( googlegeodevelopers.blogspot.com/2016/06/building-for-scale-updates-to-google.html )
* Shows a colored (classic) map or a black&white map (styled)
* Four simple shortcode parameters
* W3C valid, does not break your website's W3C validation.

**Extended version (commercial): <a title="Responsive Styled Google Maps - WordPress Plugin" href="http://codecanyon.net/item/responsive-styled-google-maps-wordpress-plugin/3909576?ref=greenline">Responsive Styled Google Maps - WordPress Plugin</a>**

* Google maps plugin to generate maps based on address OR latitude/longitude
* 100% responsive and mobile friendly
* Works in sidebars, posts, pages and custom post types
* Can create a map with multiple markers
* There can be more than one maps in a page/post
* A Google Maps API key is required since 22 June 2016 ( googlegeodevelopers.blogspot.com/2016/06/building-for-scale-updates-to-google.html )
* The plugin admin has real time map preview and shortcode preview
* Popups accept HTML code
* Search box
* Clustering option
* Show/Hide the points of interest
* Put a custom link in marker popup
* 50 included colorful map styles or a hue color
* 10 included marker icons
* You can use your own icons
* Every control on the map can be turned on/off
* Directions link
* W3C valid, does not break your website's W3C validation.
* Translation ready.
* Detailed documentation.

== Installation ==

1. Log in with admin user and go to Plugins -> Add New
2. Enter "Responsive Styled Google Maps Simplified" under search and hit Enter
3. The plugin will be the first in the search results, click "Install Now"

Or if needed, upload manually:

1. Download the plugin.
2. Unzip it locally on your computer and upload it to your web server to wp-content/plugins/
3. Open WP admin - Plugins and click "Activate" below the plugin name in the list


== Frequently Asked Questions ==

= An example of the shortcode? =

[resmap address="New York" style="1" zoom="10" height="300px" key="your_google_maps_api_key_here"]

= From where do I get the Google Maps API key? =

You can get you Google Maps API key from here: https://developers.google.com/maps/documentation/javascript/get-api-key

= What if I need more features? =

You can find more features in the extended, commercial version here: <a title="Responsive Styled Google Maps - WordPress Plugin" href="http://codecanyon.net/item/responsive-styled-google-maps-wordpress-plugin/3909576">Responsive Styled Google Maps - WordPress Plugin</a>

== Screenshots ==

1. Colored (classic) map
2. Black&white (styled) map
3. Shortcode 

== Changelog ==
= 1.5 =
* 2016/07/11
* added the possibility to use latitude, longitude instead of address
= 1.4 =
* 2016/06/27
* added the possibility to add the Google Maps API key (which is required now by Google)
= 1.3 =
* 2016/06/23
* update to the WordPress version with which it's working
= 1.2 =
* 2016/04/14
* fixed the console warnings
= 1.1 =
* 2015/10/20
* small fixes
= 1.0 =
* 2015/08/17
* initial upload

== Upgrade Notice ==

= 1.2 =
* 2016/04/14
* fixed the console warnings
= 1.1 =
* 2015/10/20
* small fixes
= 1.0 =
* 2015/08/17
* initial upload