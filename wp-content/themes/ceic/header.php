<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php endif; ?>
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	<?php wp_head(); ?>
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body <?php body_class(); ?>>
<?php get_header('side'); ?>
<div class="container-fluid navbar-superior">
	<div class="row">
		<div class="container">
			<div class="row">
				<div class="col-md-2 col-xs-4 navbar-search visible-xs-block"><?php get_search_form(true); ?></div>
				<div class="col-md-1 navbar-logo col-xs-4">
					<?php ceic_logo();?>
				</div>
				<div class="col-md-7 hidden-xs">
					<?php if ( has_nav_menu( 'superior' ) ) : ?>
					<?php wp_nav_menu( array( 
						  	'theme_location' 	=> 'superior',
						  	'container' 		=> '',
						  	'menu_class'      	=> 'nav navbar-nav col-md-12',
						  	'menu_id'         	=> '',
						  	'echo'            	=> true,
						  	'fallback_cb'     	=> 'wp_page_menu',
						  	'before'          	=> '',
						  	'after'           	=> '',
						  	'link_before'     	=> '',
						  	'link_after'      	=> '',
						  	'items_wrap'      	=> '<ul id="%1$s" class="%2$s">%3$s</ul>',
						  	'depth'           	=> 0,
						  	'walker'          	=> ''
					)); ?>
			<?php endif; ?>
				</div>
				<div class="col-md-1 navbar-search hidden-xs"><?php get_search_form(true); ?></div>
				<div class="col-md-1 col-xs-4 toggle-menu-button">
					<button type="button" class="navbar-toggle">
			          <span class="sr-only">Toggle navigation</span>
			          <span class="icon-bar"></span>
			          <span class="icon-bar"></span>
			          <span class="icon-bar"></span>
			        </button>
				</div>
				<div class="col-md-7 col-xs-12 visible-xs-block">
					<?php if ( has_nav_menu( 'superior' ) ) : ?>
					<?php wp_nav_menu( array( 
						  	'theme_location' 	=> 'superior',
						  	'container' 		=> '',
						  	'menu_class'      	=> 'nav navbar-nav col-md-12',
						  	'menu_id'         	=> '',
						  	'echo'            	=> true,
						  	'fallback_cb'     	=> 'wp_page_menu',
						  	'before'          	=> '',
						  	'after'           	=> '',
						  	'link_before'     	=> '',
						  	'link_after'      	=> '',
						  	'items_wrap'      	=> '<ul id="%1$s" class="%2$s">%3$s</ul>',
						  	'depth'           	=> 0,
						  	'walker'          	=> ''
					)); ?>
			<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
</div>