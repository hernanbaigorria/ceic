<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes(); ?>>

<head profile="http://gmpg.org/xfn/11">
<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />

<title><?php bloginfo('name'); ?> <?php if ( is_single() ) { ?> &raquo; Blog Archive <?php } ?> <?php wp_title(); ?></title>

<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="screen" />
<link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?> RSS Feed" href="<?php bloginfo('rss2_url'); ?>" />
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />

<style type="text/css" media="screen">

<?php
// Checks to see whether it needs a sidebar or not
if ( !empty($withcomments) && !is_single() ) {
?>
	#page { background: url("<?php bloginfo('stylesheet_directory'); ?>/images/kubrickbg-<?php bloginfo('text_direction'); ?>.jpg") repeat-y top; border: none; }
<?php } else { // No sidebar ?>
	#page { background: url("<?php bloginfo('stylesheet_directory'); ?>/images/kubrickbgwide.jpg") repeat-y top; border: none; }
<?php } ?>

</style>

<?php wp_head(); ?>

<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-18288601-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>

</head>
<body>



      <div id="contenedor-sitio">
      
           <div id="top">
           
             <div id="icon-social">
             	<table width="120" border="0" align="right" cellpadding="0" cellspacing="0">
  <tr>
    <td width="40" align="center" valign="middle"><a href="http://www.geic.com.ar/2010/contacto/"><img src="<?php bloginfo( 'template_url' ); ?>/imagenes/contacto.png" alt="Contacto" border="0"></a></td>
    <td width="40" align="center" valign="middle"><a href="http://www.facebook.com/pages/Grupo-de-Estudios-Internacionales-Contempor%C3%A1neos-GEIC/188026151274947?sk=wall" target="_blank"><img src="<?php bloginfo( 'template_url' ); ?>/imagenes/facebook.png" alt="GEIC en Facebook" border="0"></a></td>
    <td width="40" align="center" valign="middle"><img src="<?php bloginfo( 'template_url' ); ?>/imagenes/twitter.png"></td>
  </tr>
</table>

             </div>
             
             <div id="buscador-top">
             <?php get_search_form(); ?>

             </div>
             
             <div id="menu-top">
                  
                  <ul>
                      <li class="nada"><a href="http://www.geic.com.ar/2010/index.php">Inicio</a></li>
                      <?php wp_list_pages('title_li='); ?>
                  </ul>
             </div>


           </div>


          <!-- <div id="animacion">

            <object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,28,0" width="900" height="110">
              <param name="movie" value="<?php bloginfo( 'template_url' ); ?>/animacion/animacion.swf">
              <param name="quality" value="high">
              <embed src="<?php bloginfo( 'template_url' ); ?>/animacion/animacion.swf" quality="high" pluginspage="http://www.adobe.com/shockwave/download/download.cgi?P1_Prod_Version=ShockwaveFlash" type="application/x-shockwave-flash" width="900" height="110"></embed>
            </object>

           </div>-->
           
           <div id="menu-principal">

           <ul>
                 <?php wp_list_categories('title_li=&include=3,4,5,6,7,8,9,11,13,14,15');?>
           </ul>

           </div>



