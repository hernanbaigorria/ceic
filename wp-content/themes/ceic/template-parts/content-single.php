<?php
$classes = "col-xs-12";
?>
<article id="post-<?php the_ID(); ?>" <?php post_class($classes); ?>>
	<header class="entry-header">
		<?php 
		if (has_post_thumbnail() ) { ?>
			<div class="row">
				<div class="col-md-10 col-md-offset-1">
			<?php the_post_thumbnail('post-size', array( 'class' => 'img-responsive' )); ?>
				</div>
			</div>
		<?php } ?>
		<div class="row">
			<div class="col-md-10 col-md-offset-1">
				<?php ceic_categories(); ?>
			</div>
		</div>
		<div class="row">
		<div class="col-md-2 col-md-offset-1 author">
			<p>-<br>
			<?php $key="autor"; echo get_post_meta($post->ID, $key, true); ?><br>
			<?php the_time('d/m/Y'); ?></p>
		</div>
		<div class="col-md-6">
			<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
		</div>
		</div>
	</header><!-- .entry-header -->

	<div class="entry-content">
		<div class="row">
			<div class="col-md-2 col-md-offset-1 share">
			<p>Compartir</p>
			<!-- Go to www.addthis.com/dashboard to customize your tools -->
			<div class="addthis_inline_share_toolbox"></div>
			</div>
			<div class="col-md-6">
		<?php
			the_content();

			wp_link_pages( array(
				'before'      => '<div class="page-links"><span class="page-links-title">Paginas:</span>',
				'after'       => '</div>',
				'link_before' => '<span>',
				'link_after'  => '</span>',
				'pagelink'    => '<span class="screen-reader-text">Pagina </span>%',
				'separator'   => '<span class="screen-reader-text">, </span>',
			) );

			if ( '' !== get_the_author_meta( 'description' ) ) {
				get_template_part( 'template-parts/biography' );
			}
		?>
			</div>
		</div>
	</div><!-- .entry-content -->

	<footer class="entry-footer">
		<?php
			edit_post_link(
				sprintf(
					'Editar<span class="screen-reader-text"> "%s"</span>',
					get_the_title()
				),
				'<span class="edit-link">',
				'</span>'
			);
		?>
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->
