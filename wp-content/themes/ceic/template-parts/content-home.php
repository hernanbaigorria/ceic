<?php
/**
 * The template part for displaying content
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
$classes = array('col-md-4 col-sm-6', 'home-box');
$categories = get_the_category();
$categoriesId = array();
$categoryColor = 0;
$categoriesMayor = array(15,6,9,11);
foreach($categories as $category){
	
	if(in_array($category->cat_ID, $categoriesMayor)){
		$categoryColor = $category->cat_ID;
	}else{
		$categoriesId[] = $category->cat_ID;
	}
}
$categoryMinor = 0;
if(isset($categoriesId[0]))
{
	$categoryMinor = $categoriesId[0];
}
$category = get_category($categoryMinor);
$rl_category_color = rl_color($categoryColor);
?>

<article id="post-<?php the_ID(); ?>" <?php post_class($classes); ?> onclick="location.href='<?php echo esc_url( get_permalink() ); ?>'">
		<style><?php echo '#post-'.get_the_ID().'.home-box:hover .over'; ?>{background-color: <?php echo $rl_category_color; ?>; }</style>
		<?php the_title( sprintf( '<h2><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>
		<p class="author">-<br><?php $key="autor"; echo get_post_meta($post->ID, $key, true); ?></p>
		<span class="cat-links">
        	<div class="cat-link"><div class="cuadrado" style="background:<?php echo $rl_category_color;?>"></div><a href="<?php echo get_category_link( $category->term_id );?>" rel="category tag" style="color:<?php echo $rl_category_color;?>;"><?php echo $category->cat_name; ?></a></div>
        </span>
		<?php the_post_thumbnail('home-size', array( 'class' => 'img-responsive bg' )); ?>
		<div class="over"></div>
</article><!-- #post-## -->
