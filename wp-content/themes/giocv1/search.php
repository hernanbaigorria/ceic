<?php get_header(); ?>
	
    <div id="cuerpo-contenido">
		
        <div id="cuerpo">
		
		<?php if (have_posts()) : ?>
		
        <div id="salida-nota">
    		
            <h4>Resultados de la B&uacute;squeda</h4>		
			
			<?php while (have_posts()) : the_post(); ?>
			
            <div class="box-archivo">
    			<h3 id="post-<?php the_ID(); ?>"><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h3>
    			<small><?php the_time(__('j/m/y')); ?></small>
    		</div>

    		<?php endwhile; ?>
		
        	<div style="margin: 10px auto; float: left; width: 620px;">
                <?php next_posts_link('&laquo; Archivos m&aacute;s viejos') ?> | <?php previous_posts_link('Archivos m&aacute;s recientes &raquo;') ?>
            </div>
            
        </div>
		
		
        <br /><br />


	<?php else : ?>

		<h2 class="center">Sin resultados. Intente nuevamente utilizando otro criterio de b&uacute;squeda</h2>


	<?php endif; ?>


</div>

<?php get_footer(); ?>